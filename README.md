# GameCore

GameCore is a minigame framework for Minecraft developed to run multiple minigames on the same server. The goal of this framework is to allow minigames to be added just by dropping in a jar and minimum configuration.