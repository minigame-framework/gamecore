package com.rast.gamecore.util;

import com.rast.gamecore.GameCore;
import org.bukkit.Location;

public class Position {

    private double x;
    private double y;
    private double z;

    public Position(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Position(Location location) {
        x = location.getX();
        y = location.getY();
        z = location.getZ();
    }

    public void setPosition(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setPosition(Location location) {
        x = location.getX();
        y = location.getY();
        z = location.getZ();
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public int getBlockX() {
        return (int) x;
    }

    public int getBlockY() {
        return (int) y;
    }

    public int getBlockZ() {
        return (int) z;
    }

    public Location getLocation() {
        return new Location(GameCore.getSettings().getDefaultWorld(), x, y, z);
    }
}
