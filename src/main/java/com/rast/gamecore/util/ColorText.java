package com.rast.gamecore.util;

import org.bukkit.ChatColor;

public class ColorText {

    /**
     * Turn the color character from '&amp;' to '§' to be read by Minecraft
     *
     * @param text the text to translate
     * @return the translated text
     */
    public static String TranslateChat(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }

    /**
     * Turn the color character from '§' to '&amp;' to be read by users
     *
     * @param text the text to translate
     * @return the translated text
     */
    public static String TranslateColor(String text) {
        return text.replace(ChatColor.COLOR_CHAR, '&');
    }

    /**
     * Removes all chat formatting fromm the text
     *
     * @param text the text to remove formatting from
     * @return the clean text
     */
    public static String BleachText(String text) {
        return ChatColor.stripColor(text);
    }


}
