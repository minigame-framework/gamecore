package com.rast.gamecore.util;

import org.bukkit.entity.Player;

import java.util.Comparator;
import java.util.List;

public class ListLengthComparator implements Comparator<List<Player>> {

    /**
     * Sort the list from largest size to smallest size.
     * @param p1 the first list to compare
     * @param p2 the second list to compare
     * @return 0 if p1 list is less than or equal to p2 to else it is 1
     */
    @Override
    public int compare(List<Player> p1, List<Player> p2) {
        return p2.size()-p1.size();
    }

}
