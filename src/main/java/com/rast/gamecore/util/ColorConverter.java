package com.rast.gamecore.util;

import org.bukkit.ChatColor;
import org.bukkit.boss.BarColor;

public class ColorConverter {

    /**
     * Convert down chat color into a bar color.
     *
     * @param chatColor the ChatColor object to convert
     * @return the translated bar color
     */
    public static BarColor chatToBarColor (ChatColor chatColor) {
        switch (chatColor) {
            case DARK_BLUE:
            case DARK_AQUA:
            case AQUA:
            case BLUE:
                return BarColor.BLUE;
            case DARK_GREEN:
            case GREEN:
                return BarColor.GREEN;
            case DARK_RED:
            case RED:
                return BarColor.RED;
            case DARK_PURPLE:
                return BarColor.PURPLE;
            case LIGHT_PURPLE:
                return BarColor.PINK;
            case GOLD:
            case YELLOW:
                return BarColor.YELLOW;
            default:
                return BarColor.WHITE;
        }
    }

}
