package com.rast.gamecore.util;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Region {
    private double x1;
    private double x2;
    private double y1;
    private double y2;
    private double z1;
    private double z2;

    private void sortCords() {
        double temp;
        if (x1 > x2) {
            temp = x1;
            x1 = x2;
            x2 = temp;
        }
        if (y1 > y2) {
            temp = y1;
            y1 = y2;
            y2 = temp;
        }
        if (z1 > z2) {
            temp = z1;
            z1 = z2;
            z2 = temp;
        }
    }

    public void setRegion(Location location1, Location location2) {
        x1 = location1.getX();
        y1 = location1.getY();
        z1 = location1.getZ();
        x2 = location2.getX();
        y2 = location2.getY();
        z2 = location2.getZ();
        sortCords();
    }

    public void setRegion(Position position1, Position position2) {
        x1 = position1.getX();
        y1 = position1.getY();
        z1 = position1.getZ();
        x2 = position2.getX();
        y2 = position2.getY();
        z2 = position2.getZ();
        sortCords();
    }

    public void setRegion(double x1, double y1, double z1, double x2, double y2, double z2) {
        this.x1 = x1;
        this.y1 = y1;
        this.z1 = z1;
        this.x2 = x2;
        this.y2 = y2;
        this.z2 = z2;
        sortCords();
    }

    public Position getPosition1() {
        return new Position(x1, y1, z1);
    }

    public Position getPosition2() {
        return new Position(x2, y2, z2);
    }

    public double getWidth() {
        return x2 - x1;
    }

    public double getDepth() {
        return z2 - z1;
    }

    public double getHeight() {
        return y2 - y1;
    }

    public boolean contains(Position position) {
        if (x1 > position.getX() || position.getX() > x2) {
            return false;
        }
        if (y1 > position.getY() || position.getY() > y2) {
            return false;
        }
        return !(z1 > position.getZ()) && !(position.getZ() > z2);
    }

    public boolean contains(Location location) {
        if (x1 > location.getX() || location.getX() > x2) {
            return false;
        }
        if (y1 > location.getY() || location.getY() > y2) {
            return false;
        }
        return !(z1 > location.getZ()) && !(location.getZ() > z2);
    }

    public boolean contains(Player player) {
        Location location = player.getLocation();
        if (x1 > location.getX() || location.getX() > x2) {
            return false;
        }
        if (y1 > location.getY() || location.getY() > y2) {
            return false;
        }
        return !(z1 > location.getZ()) && !(location.getZ() > z2);
    }

    public void shiftRegion(Position position) {
        x1 += position.getX();
        x2 += position.getX();
        y1 += position.getY();
        y2 += position.getY();
        z1 += position.getZ();
        z2 += position.getZ();
    }

    public void shiftRegion(double x, double y, double z) {
        x1 += x;
        x2 += x;
        y1 += y;
        y2 += y;
        z1 += z;
        z2 += z;
    }

}
