package com.rast.gamecore.util;

import org.bukkit.Bukkit;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class DirectoryTools {

    /**
     * Copy a file and anything that its in it
     *
     * @param sourceFolder      the original file location
     * @param destinationFolder the destination file location
     * @throws IOException throws an exception if reading or writing a file fails
     */
    public static void copyFolder(File sourceFolder, File destinationFolder) throws IOException {
        // If this is a directory we want to clone it and look for more files
        if (sourceFolder.isDirectory()) {
            // If the destination folder does not exist, make it.
            if (!destinationFolder.exists()) {
                destinationFolder.mkdir();
            }

            String[] files = sourceFolder.list();

            // For every file in this directory run this same method again
            if (files != null) {
                for (String file : files) {
                    File srcFile = new File(sourceFolder, file);
                    File destFile = new File(destinationFolder, file);
                    copyFolder(srcFile, destFile);
                }
            }
        } else {
            // Copy the file content from one place to another
            Files.copy(sourceFolder.toPath(), destinationFolder.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }

    /**
     * Delete a directory
     *
     * @param folder the folder to delete
     */
    public static void deleteFolder(File folder) {
        File[] allContents = folder.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteFolder(file);
            }
        }

        if (!folder.delete()) {
            Bukkit.getLogger().warning("Could not delete file [" + folder.getAbsolutePath() + "]");
        }
    }

}
