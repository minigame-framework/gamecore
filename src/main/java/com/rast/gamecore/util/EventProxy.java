package com.rast.gamecore.util;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameWorld;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Set;

public abstract class EventProxy {

    /**
     * Test to see if this event is within the jurisdiction of the minigame
     *
     * @param player the player in the event
     * @param world  the world needed to be valid
     * @return true if the event is within the jurisdiction or false if it is out if the jurisdiction
     */
    public static boolean isValid(Player player, World world) {
        return player.getWorld().equals(world);
    }

    /**
     * Test to see if this event is within the jurisdiction of the minigame
     *
     * @param player the player in the event
     * @param group  the group needed to be valid
     * @return true if the event is within the jurisdiction or false if it is out if the jurisdiction
     */
    public static boolean isValid(Player player, String group) {
        return GameCore.getGameMaster().getGroupPlayers(group).contains(player);
    }

    /**
     * Test to see if this event is within the jurisdiction of the minigame
     *
     * @param entity the entity in the event
     * @param world  the world needed to be valid
     * @return true if the event is within the jurisdiction or false if it is out if the jurisdiction
     */
    public static boolean isValid(Entity entity, World world) {
        return entity.getWorld().equals(world);
    }

    /**
     * Test to see if this event is within the jurisdiction of the minigame
     *
     * @param player the player in the event
     * @param world  the world needed to be valid
     * @param group  the group that the player must be in
     * @return true if the event is within the jurisdiction or false if it is out if the jurisdiction
     */
    public static boolean isValid(Player player, World world, String group) {
        return player.getWorld().equals(world) && GameCore.getGameMaster().getGroupPlayers(group).contains(player);
    }

    /**
     * Test to see if this event is within the jurisdiction of the minigame
     *
     * @param player the player in the event
     * @param world  the world needed to be valid
     * @param groups the groups that the player must be in (Inclusively)
     * @return true if the event is within the jurisdiction or false if it is out if the jurisdiction
     */
    public static boolean isValidInclusive(Player player, World world, Set<String> groups) {

        if (player.getWorld().equals(world)) {
            for (String group : groups) {
                if (!GameCore.getGameMaster().getGroupPlayers(group).contains(player)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Test to see if this event is within the jurisdiction of the minigame
     *
     * @param player the player in the event
     * @param world  the world needed to be valid
     * @param groups the groups that the player must be in (Exclusively)
     * @return true if the event is within the jurisdiction or false if it is out if the jurisdiction
     */
    public static boolean isValidExclusive(Player player, World world, Set<String> groups) {

        if (player.getWorld().equals(world)) {
            for (String group : groups) {
                if (!GameCore.getGameMaster().getGroupPlayers(group).contains(player)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    /**
     * Test to see if this event is within the jurisdiction of the minigame
     *
     * @param player     the player in the event
     * @param gameWorlds the game worlds needed to be valid
     * @return true if the event is within the jurisdiction or false if it is out if the jurisdiction
     */
    public static boolean isValid(Player player, Set<GameWorld> gameWorlds) {
        for (GameWorld gameWorld : gameWorlds) {
            if (gameWorld.getBukkitWorld().equals(player.getWorld())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Test to see if this event is within the jurisdiction of the minigame
     *
     * @param entity     the entity in the event
     * @param gameWorlds the game worlds needed to be valid
     * @return true if the event is within the jurisdiction or false if it is out if the jurisdiction
     */
    public static boolean isValid(Entity entity, Set<GameWorld> gameWorlds) {
        for (GameWorld gameWorld : gameWorlds) {
            if (gameWorld.getBukkitWorld().equals(entity.getWorld())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Test to see if this event is within the jurisdiction of the minigame
     *
     * @param player     the player in the event
     * @param gameWorlds the game worlds needed to be valid
     * @param group      the group that the player must be in
     * @return true if the event is within the jurisdiction or false if it is out if the jurisdiction
     */
    public static boolean isValid(Player player, Set<GameWorld> gameWorlds, String group) {
        for (GameWorld gameWorld : gameWorlds) {
            if (gameWorld.getBukkitWorld().equals(player.getWorld()) && GameCore.getGameMaster().getGroupPlayers(group).contains(player)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Test to see if this event is within the jurisdiction of the minigame
     *
     * @param player     the player in the event
     * @param gameWorlds the game worlds needed to be valid
     * @param groups     the groups that the player must be in (Inclusively)
     * @return true if the event is within the jurisdiction or false if it is out if the jurisdiction
     */
    public static boolean isValidInclusive(Player player, Set<GameWorld> gameWorlds, Set<String> groups) {
        for (GameWorld gameWorld : gameWorlds) {
            if (gameWorld.getBukkitWorld().equals(player.getWorld())) {
                for (String group : groups) {
                    if (!GameCore.getGameMaster().getGroupPlayers(group).contains(player)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Test to see if this event is within the jurisdiction of the minigame
     *
     * @param player     the player in the event
     * @param gameWorlds the game worlds needed to be valid
     * @param groups     the groups that the player must be in (Exclusively)
     * @return true if the event is within the jurisdiction or false if it is out if the jurisdiction
     */
    public static boolean isValidExclusive(Player player, Set<GameWorld> gameWorlds, Set<String> groups) {
        for (GameWorld gameWorld : gameWorlds) {
            if (gameWorld.getBukkitWorld().equals(player.getWorld())) {
                for (String group : groups) {
                    if (GameCore.getGameMaster().getGroupPlayers(group).contains(player)) {
                        return true;
                    }
                }
                return false;
            }
        }
        return false;
    }

}
