package com.rast.gamecore.util;

import com.rast.gamecore.GameCore;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class MatchMaker {

    /**
     * Separate players into teams prioritising friends when possible making teams as even as possible.
     *
     * @param playerPool the pool of players to choose from
     * @param teams      how many teams should be made? More than one team is required.
     * @return a list containing lists of players
     */
    public static List<List<Player>> placePlayersOnTeams(List<Player> playerPool, int teams) {
        List<List<Player>> teamList = new ArrayList<>();
        for (int i = 0; i < teams; i++) {
            teamList.add(new ArrayList<>());
        }
        List<List<Player>> friendLists = new ArrayList<>();

        // generate friend lists
        for (Player player : playerPool) {

            boolean playerExists = false;

            // see if the player is already in the friend list
            for (List<Player> friendList : friendLists) {
                if (friendList.contains(player)) {
                    playerExists = true;
                    break;
                }
            }

            // add the player if they do not exist in the list already
            if (!playerExists) {

                List<Player> newFriendList = new ArrayList<>();
                newFriendList.add(player);
                Set<UUID> friends = GameCore.getFriendManager().getFriends(player);

                // see if the friend is in the playerPool
                for (Player friendPlayer : playerPool) {
                    if (friends.contains(friendPlayer.getUniqueId())) {

                        boolean friendExists = false;

                        for (List<Player> friendSet : friendLists) {
                            if (friendSet.contains(friendPlayer)) {
                                friendExists = true;
                                break;
                            }
                        }

                        // if the friend does not yet exist add them
                        if (!friendExists) {
                            newFriendList.add(friendPlayer);
                        }
                    }
                }

                // add this new friend list to the friend lists
                friendLists.add(newFriendList);
            }
        }

        // figure out each friend list length and get them close to even for each team
        friendLists.sort(new ListLengthComparator());
        for (List<Player> friendList : friendLists) {
            int minSize = teamList.get(0).size();
            int team = 0;
            for (int i = 0; i < teams; i++) {
                if (minSize != Math.min(minSize, teamList.get(i).size())) {
                    minSize = teamList.get(i).size();
                    team = i;
                }
            }
            teamList.get(team).addAll(friendList);
        }

        // now balance the teams near perfectly
        int averageSize = 0;
        double sum = 0;
        List<Player> snipedPlayers = new ArrayList<>();

        for (int i = 0; i < teams; i++) {
            sum += teamList.get(i).size();
        }
        averageSize = (int) Math.ceil(sum / teams);

        // snip the teams to the average
        for (List<Player> team : teamList) {
            if (team.size() > averageSize) {
                snipedPlayers.addAll(team.subList(averageSize, team.size()));
                team.removeAll(team.subList(averageSize, team.size()));
            }
        }

        // redistribute the sniped players to teams that need them
        for (List<Player> team : teamList) {
            if (team.size() < averageSize && !snipedPlayers.isEmpty()) {
                team.addAll(snipedPlayers.subList(0, Math.min(snipedPlayers.size(), (averageSize - team.size()))));
                team.removeAll(team.subList(averageSize, team.size()));
            }
        }

        return teamList;
    }
}
