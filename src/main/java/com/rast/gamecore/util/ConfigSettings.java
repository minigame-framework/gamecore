package com.rast.gamecore.util;

public abstract class ConfigSettings {

    public ConfigSettings() {
        reload();
    }

    public abstract void reload();
}
