package com.rast.gamecore.util;

import org.bukkit.entity.Player;

public class CleanPlayer {

    /**
     * Clean up the player's health, hunger, levels, and inventory.
     * @param player the player to clean up
     */
    public static void cleanAll(Player player) {
        cleanHealth(player);
        cleanLevInv(player);
    }

    /**
     * Clean up the player's health and hunger
     * @param player the player to clean up
     */
    public static void cleanHealth(Player player) {
        player.setHealth(20);
        player.setFoodLevel(20);
        player.setFireTicks(0);
    }

    /**
     * Clean up the player's levels and inventory
     * @param player the player to clean up
     */
    public static void cleanLevInv(Player player) {
        player.getInventory().clear();
        player.setLevel(0);
        player.setExp(0f);
    }

}
