package com.rast.gamecore.util;

// Custom item tags for com.rast.gamecore.util.Itemtags
public enum ItemTag {
    WATER, AIR, FLUID
}
