package com.rast.gamecore.util;

import com.rast.gamecore.GameCore;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommandManager implements CommandExecutor, TabCompleter {


    private final HashMap<String, Subcommand> subcommands;

    public CommandManager(JavaPlugin plugin, HashMap<String, Subcommand> subcommands) {
        this.subcommands = subcommands;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        Subcommand subcommand;

        if (args.length == 0) {
            subcommand = subcommands.get("help");
        } else {
            subcommand = subcommands.get(args[0]);
            if (subcommand == null)
                subcommand = subcommands.get("help");
        }
        if (!sender.hasPermission(subcommand.getPermission())) {
            sender.sendMessage(GameCore.getLocale().getErrorNoPermission());
            return true;
        }
        return subcommand.onCommand(sender, cmd, label, args);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {

        Subcommand subcommand;

        if (args.length == 1) {
            List<String> keySet = new ArrayList<>(subcommands.keySet());
            List<String> tabList = new ArrayList<>();
            for (String key : keySet) {
                if (sender.hasPermission(subcommands.get(key).getPermission()))
                    tabList.add(key);
            }

            return tabList;
        } else {
            subcommand = subcommands.get(args[0]);

            if (subcommand == null)
                subcommand = subcommands.get("help");
        }

        return subcommand.onTabComplete(sender, cmd, label, args);

    }
}