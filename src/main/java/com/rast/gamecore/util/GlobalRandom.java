package com.rast.gamecore.util;

import java.util.Random;

public class GlobalRandom {

    private final Random random;

    /**
     * Initiate the global random.
     */
    public GlobalRandom() {
        random = new Random(System.currentTimeMillis());
    }

    /**
     * Create a random object with a random seed to use across all games.
     *
     * @return the global random object
     */
    public Random getRandom() {
        return random;
    }

}
