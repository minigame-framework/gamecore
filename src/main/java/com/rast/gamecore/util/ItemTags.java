package com.rast.gamecore.util;

import org.bukkit.Material;

import java.util.HashSet;
import java.util.Set;

public class ItemTags {
    /**
     * Returns a collection of materials under a custom tag
     * This is useful because it adds groups of materials that are currently not supplied by spigot's Tags
     * This may need to be updated in future versions of Minecraft due to block changes
     *
     * @param tag the tag for the materials you want
     * @return the collection of tagged materials
     */
    // TODO May need to update in new minecraft versions
    public static Set<Material> getTag(ItemTag tag) {

        Set<Material> cTag = new HashSet<>();

        switch (tag) {
            case AIR:
                cTag.add(Material.AIR);
                cTag.add(Material.CAVE_AIR);
                cTag.add(Material.VOID_AIR);
                break;
            case WATER:
                cTag.add(Material.WATER);
                cTag.add(Material.BUBBLE_COLUMN);
                break;
            case FLUID:
                cTag.add(Material.AIR);
                cTag.add(Material.CAVE_AIR);
                cTag.add(Material.VOID_AIR);
                cTag.add(Material.WATER);
                cTag.add(Material.BUBBLE_COLUMN);
                cTag.add(Material.LAVA);
                break;
        }
        return cTag;
    }
}
