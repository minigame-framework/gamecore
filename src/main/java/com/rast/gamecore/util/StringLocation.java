package com.rast.gamecore.util;

import com.rast.gamecore.GameCore;
import org.bukkit.Location;

public class StringLocation {
    /**
     * Turn a location into a string format
     * The format is '[Location] x:# y:# z:# pitch:# yaw:#'
     *
     * @param loc the location to convert
     * @return the location as a string
     */
    public static String toString(Location loc) {
        // [Location] x:# y:# z:# pitch:# yaw:#
        return ("[Location] x:" + loc.getX() + " y:" + loc.getY() + " z:" + loc.getZ() + " pitch:" + loc.getPitch() + " yaw:" + loc.getYaw());
    }

    /**
     * Turn a string to a location
     * The format iss '[Location] x:# y:# z:# pitch:# yaw:#'
     *
     * @param st the string to convert
     * @return the location from the string
     */
    public static Location toLocation(String st) {

        double x;
        double y;
        double z;
        float pitch;
        float yaw;

        String[] split = st.split(":");
        x = Double.parseDouble(split[1].split(" ")[0]);
        y = Double.parseDouble(split[2].split(" ")[0]);
        z = Double.parseDouble(split[3].split(" ")[0]);
        pitch = Float.parseFloat(split[4].split(" ")[0]);
        yaw = Float.parseFloat(split[5].split(" ")[0]);

        Location loc = new Location(GameCore.getSettings().getDefaultWorld(), x, y, z);
        loc.setPitch(pitch);
        loc.setYaw(yaw);

        return loc;
    }
}
