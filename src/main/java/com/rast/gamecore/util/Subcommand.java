package com.rast.gamecore.util;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public abstract class Subcommand {

    public final JavaPlugin plugin;

    public Subcommand(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    public abstract String getHelp();

    public abstract String getUsage(String label);

    public abstract String getPermission();

    public abstract boolean onCommand(CommandSender sender, Command cmd, String label, String[] args);

    public abstract List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args);
}
