package com.rast.gamecore.util;

import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class BroadcastWorld {

    /**
     * Broadcast a message to all the players in the world.
     *
     * @param world world
     * @param message a string message
     */
    public static void broadcastChat(World world, String message) {
        for (Player player : world.getPlayers()) {
            player.sendMessage(message);
        }
    }

    /**
     * Broadcast a sound to all the players in the world
     *
     * @param world world
     * @param sound sound
     * @param pitch pitch
     */
    public static void broadcastSound(World world, Sound sound, float pitch) {
        for (Player player : world.getPlayers()) {
            player.playSound(player.getLocation(), sound, SoundCategory.MASTER, Float.MAX_VALUE, pitch);
        }
    }
}
