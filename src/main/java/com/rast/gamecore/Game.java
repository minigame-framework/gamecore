package com.rast.gamecore;

import com.rast.gamecore.util.CleanPlayer;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import java.io.IOException;
import java.util.*;

public abstract class Game {

    private final String name;
    private final JavaPlugin plugin;
    private final List<String> maps;
    private final boolean canJoinWhenRunning;

    private final Set<Player> playerSet = new HashSet<>();

    private final HashMap<String, MapConfig> mapConfigs = new HashMap<>();
    private GameInstanceManager gameInstanceManager;

    /**
     * Initiate the game class.
     *
     * @param name the name of the game
     * @param mapList the list of maps for the game (same as file names)
     * @param canJoinWhenRunning can new players join when the game is running?
     * @param plugin the hosting the game
     */
    public Game(String name, List<String> mapList, boolean canJoinWhenRunning, JavaPlugin plugin) {
        this.name = name;
        this.plugin = plugin;
        this.canJoinWhenRunning = canJoinWhenRunning;
        maps = mapList;
        Collections.sort(maps);
    }

    /**
     * Initiate the game class.
     *
     * @param name the name of the game
     * @param mapList the list of maps for the game (same as file names)
     * @param plugin the hosting the game
     */
    public Game(String name, List<String> mapList, JavaPlugin plugin) {
        this.name = name;
        this.plugin = plugin;
        canJoinWhenRunning = false;
        maps = mapList;
        Collections.sort(maps);
    }

    /**
     * Set the game instance manager to be used by the game.
     * This should be set as soon as possible but shouldn't be an issue because
     * the GameInstanceManager should do this automatically.
     *
     * @param gameInstanceManager the game instance manager for this game
     */
    public void setGameInstanceManager(GameInstanceManager gameInstanceManager) {
        this.gameInstanceManager = gameInstanceManager;
    }

    /**
     * Add a map config to the game.
     * All map configs should be added for all maps set for this game.
     *
     * @param mapConfig the map config to add
     */
    public void addMapConfig(MapConfig mapConfig) {
        mapConfigs.put(mapConfig.getName(), mapConfig);
    }

    /**
     * Add a list of map configs to the game.
     * All map configs should be added for all maps set for this game.
     *
     * @param mapConfigs the map config to add
     */
    public void addMapConfigs(List<MapConfig> mapConfigs) {
        for (MapConfig mapConfig : mapConfigs) {
            this.mapConfigs.put(mapConfig.getName(), mapConfig);
        }
    }

    /**
     * Send a player to a specified game world.
     *
     * @param player the player to send
     * @param gameWorld the game world to send to
     */
    public void sendPlayer(Player player, GameWorld gameWorld) {
        // if this player is already in this game, we want to reject them
        if (getPlayerSet().contains(player)) {
            return;
        }

        // get the game instance from the game world
        GameInstance instance = getGameInstanceManager().getInstanceFromWorld(gameWorld.getBukkitWorld());

        // this game template only allows players to join maps that are with the tag "WAITING_OPEN" but this can be changed
        if (instance != null && (gameWorld.getStatus().equals(GameStatus.WAITING_OPEN) || (gameWorld.getStatus().equals(GameStatus.RUNNING_OPEN) && canJoinWhenRunning))) {
            processNewPlayer(player, instance); // presses the player because this map is open to new players
        } else {
            if (gameWorld.getStatus().equals(GameStatus.WAITING_CLOSED)) {
                player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getErrorGameFull());
                GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame()).sendPlayer(player);
            }

            if (gameWorld.getStatus().equals(GameStatus.RUNNING_OPEN) || gameWorld.getStatus().equals(GameStatus.RUNNING_CLOSED)) {
                player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getErrorGameRunning());
                GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame()).sendPlayer(player);
            }

            if (instance == null) {
                player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getErrorNoGame());
                GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame()).sendPlayer(player);
            }
        }
    }

    /**
     * Send a player to a game world with a specified map.
     * If no game world is found with the map, a new world is created with the map.
     * This will send the player most full game that is not full.
     *
     * @param player the player to send
     * @param map the map to send the player to
     */
    public void sendPlayer(Player player, String map) {
        // if this player is already in this game, we want to reject them
        if (getPlayerSet().contains(player)) {
            return;
        }

        // if the map does not exist, do not look for worlds with the map
        if (!getMapSet().contains(map)) {
            sendPlayer(player);
            return;
        }

        GameInstance fullestInstance = null;

        for (GameInstance instance : getGameInstanceManager().getGameInstances()) {
            if (instance.getGameWorld().getMap().equals(map)) {
                if (instance.getGameWorld().getStatus().equals(GameStatus.WAITING_OPEN)) {
                    if (fullestInstance != null &&
                            (((double) fullestInstance.getGameWorld().getPlayerCount())/fullestInstance.getGameWorld().getMaxPlayerCount()) <
                                    (((double) instance.getGameWorld().getPlayerCount())/instance.getGameWorld().getMaxPlayerCount())) {
                        fullestInstance = instance;
                    } else if (fullestInstance == null){
                        fullestInstance = instance;
                    }
                }
            }
        }

        if (fullestInstance != null) {
            processNewPlayer(player, fullestInstance); // presses the player because this map is open to new players
            return;
        }

        // we could no find a world for this player so we shall create a new one
        try {
            GameWorld newWorld = new GameWorld(this, map, mapConfigs.get(map).getMaxPlayers(), plugin);
             GameInstance newInstance = getNewGameInstance(newWorld);
            if (gameInstanceManager.addInstance(newInstance)) {
                player.sendMessage(ChatColor.RED + "The world you were attempting to join is not valid.");
                GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame()).sendPlayer(player);
                return;
            }
            processNewPlayer(player, newInstance);
        } catch (IOException e) {
            e.printStackTrace();
            player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getInternalFileError());
            GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame()).sendPlayer(player);
        }
    }

    /**
     * Send a player to this game.
     * This will send the player most full game that is not full.
     *
     * @param player the player to send
     */
    public void sendPlayer(Player player) {
        // if this player is already in this game, we want to reject them
        if (getPlayerSet().contains(player)) {
            return;
        }

        GameInstance fullestInstance = null;

        for (GameInstance instance : gameInstanceManager.getGameInstances()) {
            // try to add player to the fullest game by percent
            if (instance.getGameWorld().getStatus().equals(GameStatus.WAITING_OPEN)
                    || (instance.getGameWorld().getStatus().equals(GameStatus.RUNNING_OPEN) && canJoinWhenRunning)) {
                if (fullestInstance != null &&
                        (((double) fullestInstance.getGameWorld().getPlayerCount())/fullestInstance.getGameWorld().getMaxPlayerCount()) <
                                (((double) instance.getGameWorld().getPlayerCount())/instance.getGameWorld().getMaxPlayerCount())) {
                    fullestInstance = instance;
                } else if (fullestInstance == null){
                    fullestInstance = instance;
                }
            }
        }

        if (fullestInstance != null) {
            processNewPlayer(player, fullestInstance); // presses the player because this map is open to new players
            return;
        }

        // we could no find a world for this player so we shall create a new one
        try {
            String map = getRandomMap();
            MapConfig mapConfig = mapConfigs.get(map);
            if (mapConfig == null) {
                player.sendMessage(ChatColor.RED + "The map " + map + " was not configured! Tell a moderator to configure this map or remove it from the maps folder.");
                GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame()).sendPlayer(player);
                return;
            }
            GameWorld newWorld = new GameWorld(this, map, mapConfigs.get(map).getMaxPlayers(), plugin);
            GameInstance newInstance = getNewGameInstance(newWorld);

            if (getGameInstanceManager().addInstance(newInstance)) {
                player.sendMessage(ChatColor.RED + "The world you were attempting to join is not valid.");
                GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame()).sendPlayer(player);
                return;
            }
            processNewPlayer(player, newInstance);
        } catch (IOException e) {
            e.printStackTrace();
            player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getInternalFileError());
        }
    }

    /**
     * Send a player to spectate the game.
     *
     * @param player the player to send
     * @param gameWorld the game world to send to
     */
    public void sendSpectator(Player player, GameWorld gameWorld) {
        GameInstance instance = getGameInstanceManager().getInstanceFromWorld(gameWorld.getBukkitWorld());
        if (instance != null) {
            player.setGameMode(GameMode.SPECTATOR);
            Location loc = mapConfigs.get(gameWorld.getMap()).getGameSpawn();
            loc.setWorld(gameWorld.getBukkitWorld());
            player.teleport(loc);
            player.getInventory().clear();
        }
    }

    /**
     * Pull a player from the game.
     *
     * @param player player to pull
     */
    public void pullPlayer(Player player) {
        GameMaster gameMaster = GameCore.getGameMaster();
        playerSet.remove(player);
        gameMaster.removePlayerFromGroup(name, player);
        gameMaster.removePlayerGame(player);
        gameInstanceManager.wipePlayer(player);
    }

    /**
     * Run generic possessing on a player before sending them to the game.
     * After possessing, send the player to a game world.
     *
     * @param player the player to possess
     * @param instance the game instance to add the player to
     */
    public void processNewPlayer(Player player, GameInstance instance) {
        GameMaster gameMaster = GameCore.getGameMaster();
        gameMaster.addToPlayerGroup(name, player);
        gameMaster.setPlayerGame(player, this);
        playerSet.add(player);
        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
        CleanPlayer.cleanAll(player);
        gameInstanceManager.addPlayerToInstance(player, instance);
    }

    /**
     * Get a sorted list of game instances in a certain order.
     * Order: [WAITING_OPEN, RUNNING_OPEN, WAITING_CLOSED, RUNNING_CLOSED]
     *
     * @return the sorted instance list
     */
    public List<GameWorld> getSortedGameList() {
        List<GameWorld> gameList = new ArrayList<>();
        for (GameWorld gameWorld : getGameSet()) {
            if (gameWorld.getStatus().equals(GameStatus.WAITING_OPEN)) {
                gameList.add(gameWorld);
            }
        }
        for (GameWorld gameWorld : getGameSet()) {
            if (gameWorld.getStatus().equals(GameStatus.RUNNING_OPEN)) {
                gameList.add(gameWorld);
            }
        }
        for (GameWorld gameWorld : getGameSet()) {
            if (gameWorld.getStatus().equals(GameStatus.WAITING_CLOSED)) {
                gameList.add(gameWorld);
            }
        }
        for (GameWorld gameWorld : getGameSet()) {
            if (gameWorld.getStatus().equals(GameStatus.RUNNING_CLOSED)) {
                gameList.add(gameWorld);
            }
        }
        return gameList;
    }

    /**
     * Get a sorted list of open game instances in a certain order.
     * Order: [WAITING_OPEN, RUNNING_OPEN]
     *
     * @return the sorted instance list
     */
    public List<GameWorld> getSortedOpenGameList() {
        List<GameWorld> gameList = new ArrayList<>();
        for (GameWorld gameWorld : getGameSet()) {
            if (gameWorld.getStatus().equals(GameStatus.WAITING_OPEN)) {
                gameList.add(gameWorld);
            }
        }
        for (GameWorld gameWorld : getGameSet()) {
            if (gameWorld.getStatus().equals(GameStatus.RUNNING_OPEN)) {
                gameList.add(gameWorld);
            }
        }
        return gameList;
    }

    /**
     * Get the set of game world that are apart of this Game
     *
     * @return a set of game worlds
     */
    public Set<GameWorld> getGameSet() {
        Set<GameWorld> gameSet = new HashSet<>();
        for (GameInstance instance : gameInstanceManager.getGameInstances()) {
            gameSet.add(instance.getGameWorld());
        }
        return gameSet;
    }

    /**
     * Get a game world from a world.
     *
     * @param world world to get a game world from.
     * @return a game world that has this world if one is found, if non was found then it will return null
     */
    public GameWorld getGameWorld(World world) {
        for (GameWorld gameWorld : getGameSet()) {
            if (gameWorld.getBukkitWorld().equals(world)) {
                return gameWorld;
            }
        }
        return null;
    }

    /**
     * Get the name of the game.
     *
     * @return the game's name
     */
    public String getName() {
        return name;
    }

    /**
     * Get the game instance manager that is managing game instances for this game.
     *
     * @return the game instance manager
     */
    public GameInstanceManager getGameInstanceManager() {
        return gameInstanceManager;
    }

    /**
     * Get a set of players that are in the game.
     *
     * @return a set of players in the game
     */
    public Set<Player> getPlayerSet() {
        return playerSet;
    }

    /**
     * Get a set of maps for the game.
     * This is a set of map names.
     *
     * @return a set of maps for the game
     */
    public Set<String> getMapSet() {
        return new HashSet<>(maps);
    }

    /**
     * Get a sorted list of maps in alphabetical order.
     * This is a set of map names.
     *
     * @return a set of maps for the game sorted.
     */
    public List<String> getSortedMapList() {
        return maps;
    }

    /**
     * Get a random map from the game's list of maps.
     *
     * @return a random map
     */
    public String getRandomMap() {
        Random rnd = new Random(System.currentTimeMillis()); // create a new random
        int index = rnd.nextInt(maps.size()); // select a random number from the elements in the list
        return maps.get(index); // return the element
    }

    public abstract String getChatFormat(Player player);

    public abstract GameInstance getNewGameInstance(GameWorld gameWorld);
}
