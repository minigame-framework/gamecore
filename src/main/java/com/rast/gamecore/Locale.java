package com.rast.gamecore;

import com.rast.gamecore.util.ColorText;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class Locale {

    private String systemPrefix, gameInfo, welcomeMessage, noTalkSpectate, errorNoPermission, errorTeamFull, errorGameFull,
            errorGameRunning, errorNoGame, errorNoMap, internalFileError;
    private String friendsPrefix, friendNoPlayerSpecified, friendsUserNotFound, friendsList, friendsListOnline,
            friendsAccept, friendsDecline, friendsRemoved, friendsInvite, friendsAcceptOther, friendsDeclineOther,
            friendsRemovedOther, friendsInviteOther, friendsFull, errorFriendSelf, errorFriendDisabled;
    private String partyPrefix, partyNoPlayerSpecified, partyUserNotFound, partyList, partyCreate,
            partyAccept, partyDecline, partyLeave, partyInvite, partyKick, partyJoin, partyAcceptOther, partyDeclineOther,
            partyLeaveOther, partyInviteOther, partyKickOther, partyKickAll, partyJoinOther,
            errorStillInParty, errorNeedOwnParty, errorNoParty, errorInviteSelf, partyDisband, partySend, partySendOther;

    /**
     * The gamecore locale
     */
    public Locale() {
        reload();
    }

    public void reload() {
        // Get the plugin, reload config, and get the config
        GameCore plugin = GameCore.getPlugin();

        // Get the locale file
        File configFile = new File(plugin.getDataFolder().getPath() + "/locale.yml");

        // Make the directory if it does not exist yet
        if (!configFile.exists()) {
            configFile.getParentFile().mkdirs();
            plugin.saveResource("locale.yml", false);
        }

        // Load the locale
        FileConfiguration locale = new YamlConfiguration();

        try {
            locale.load(configFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }

        // Save defaults
        BufferedReader reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(plugin.getResource("locale.yml"))));

        try {
            FileConfiguration defaults = new YamlConfiguration();
            defaults.load(reader);
            locale.setDefaults(defaults);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }

        // Grab all the locale
        systemPrefix = ColorText.TranslateChat(locale.getString("system-prefix"));
        gameInfo = ColorText.TranslateChat(locale.getString("game-info"));
        noTalkSpectate = ColorText.TranslateChat(locale.getString("no-talk-spectate"));
        welcomeMessage = ColorText.TranslateChat(locale.getString("welcome-message"));
        errorNoPermission = ColorText.TranslateChat(locale.getString("error-no-permission"));
        errorTeamFull = ColorText.TranslateChat(locale.getString("error-team-full"));
        errorGameFull = ColorText.TranslateChat(locale.getString("error-game-full"));
        errorGameRunning = ColorText.TranslateChat(locale.getString("error-game-running"));
        errorNoGame = ColorText.TranslateChat(locale.getString("error-no-game"));
        errorNoMap = ColorText.TranslateChat(locale.getString("error-no-map"));
        internalFileError = ColorText.TranslateChat(locale.getString("internal-file-error"));

        friendsPrefix = ColorText.TranslateChat(locale.getString("friends-prefix"));
        friendNoPlayerSpecified = ColorText.TranslateChat(locale.getString("friends-no-specified-player"));
        friendsUserNotFound = ColorText.TranslateChat(locale.getString("friends-user-not-found"));
        friendsList = ColorText.TranslateChat(locale.getString("friends-list"));
        friendsListOnline = ColorText.TranslateChat(locale.getString("friends-list-online"));
        friendsAccept = ColorText.TranslateChat(locale.getString("friends-accept"));
        friendsDecline = ColorText.TranslateChat(locale.getString("friends-deny"));
        friendsRemoved = ColorText.TranslateChat(locale.getString("friends-removed"));
        friendsInvite = ColorText.TranslateChat(locale.getString("friends-invite"));
        friendsAcceptOther = ColorText.TranslateChat(locale.getString("friends-accept-other"));
        friendsDeclineOther = ColorText.TranslateChat(locale.getString("friends-deny-other"));
        friendsRemovedOther = ColorText.TranslateChat(locale.getString("friends-removed-other"));
        friendsInviteOther = ColorText.TranslateChat(locale.getString("friends-invite-other"));
        friendsFull = ColorText.TranslateChat(locale.getString("friends-full"));
        errorFriendSelf = ColorText.TranslateChat(locale.getString("error-friend-self"));
        errorFriendDisabled = ColorText.TranslateChat(locale.getString("error-friend-disabled"));

        partyPrefix = ColorText.TranslateChat(locale.getString("party-prefix"));
        partyNoPlayerSpecified = ColorText.TranslateChat(locale.getString("party-no-specified-player"));
        partyUserNotFound = ColorText.TranslateChat(locale.getString("party-user-not-found"));
        partyList = ColorText.TranslateChat(locale.getString("party-list"));
        partyCreate = ColorText.TranslateChat(locale.getString("party-create"));
        partyAccept = ColorText.TranslateChat(locale.getString("party-accept"));
        partyDecline = ColorText.TranslateChat(locale.getString("party-deny"));
        partyLeave = ColorText.TranslateChat(locale.getString("party-leave"));
        partyJoin = ColorText.TranslateChat(locale.getString("party-join"));
        partyInvite = ColorText.TranslateChat(locale.getString("party-invite"));
        partyKick = ColorText.TranslateChat(locale.getString("party-kick"));
        partyAcceptOther = ColorText.TranslateChat(locale.getString("party-accept-other"));
        partyDeclineOther = ColorText.TranslateChat(locale.getString("party-deny-other"));
        partyLeaveOther = ColorText.TranslateChat(locale.getString("party-leave-other"));
        partyInviteOther = ColorText.TranslateChat(locale.getString("party-invite-other"));
        partyJoinOther = ColorText.TranslateChat(locale.getString("party-join-other"));
        partyKickOther = ColorText.TranslateChat(locale.getString("party-kick-other"));
        partyKickAll = ColorText.TranslateChat(locale.getString("party-kick-all"));
        errorStillInParty = ColorText.TranslateChat(locale.getString("error-still-in-party"));
        errorNeedOwnParty = ColorText.TranslateChat(locale.getString("error-need-own-party"));
        errorNoParty = ColorText.TranslateChat(locale.getString("error-no-party"));
        partyDisband = ColorText.TranslateChat(locale.getString("party-disband"));
        errorInviteSelf = ColorText.TranslateChat(locale.getString("error-invite-self"));
        partySend = ColorText.TranslateChat(locale.getString("party-send"));
        partySendOther = ColorText.TranslateChat(locale.getString("party-send-other"));
    }

    public String getErrorNoMap() {
        return errorNoMap;
    }

    public String getErrorNoGame() {
        return errorNoGame;
    }

    public String getErrorTeamFull() {
        return errorTeamFull;
    }

    public String getErrorGameFull() {
        return errorGameFull;
    }

    public String getErrorGameRunning() {
        return errorGameRunning;
    }

    public String getErrorNoPermission() {
        return errorNoPermission;
    }

    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    public String getSystemPrefix() {
        return systemPrefix;
    }

    public String getInternalFileError() {
        return internalFileError;
    }

    public String getFriendsPrefix() {
        return friendsPrefix;
    }

    public String getFriendNoPlayerSpecified() {
        return friendNoPlayerSpecified;
    }

    public String getFriendsUserNotFound() {
        return friendsUserNotFound;
    }

    public String getFriendsList() {
        return friendsList;
    }

    public String getFriendsListOnline() {
        return friendsListOnline;
    }

    public String getFriendsAccept() {
        return friendsAccept;
    }

    public String getFriendsDecline() {
        return friendsDecline;
    }

    public String getFriendsRemoved() {
        return friendsRemoved;
    }

    public String getFriendsInvite() {
        return friendsInvite;
    }

    public String getFriendsAcceptOther() {
        return friendsAcceptOther;
    }

    public String getFriendsDeclineOther() {
        return friendsDeclineOther;
    }

    public String getFriendsRemovedOther() {
        return friendsRemovedOther;
    }

    public String getFriendsInviteOther() {
        return friendsInviteOther;
    }

    public String getFriendsFull() {
        return friendsFull;
    }

    public String getErrorFriendDisabled() {
        return errorFriendDisabled;
    }

    public String getPartyPrefix() {
        return partyPrefix;
    }

    public String getPartyNoPlayerSpecified() {
        return partyNoPlayerSpecified;
    }

    public String getPartyUserNotFound() {
        return partyUserNotFound;
    }

    public String getPartyList() {
        return partyList;
    }

    public String getPartyCreate() {
        return partyCreate;
    }

    public String getPartyAccept() {
        return partyAccept;
    }

    public String getPartyDecline() {
        return partyDecline;
    }

    public String getPartyLeave() {
        return partyLeave;
    }

    public String getPartyInvite() {
        return partyInvite;
    }

    public String getPartyKick() {
        return partyKick;
    }

    public String getPartyJoin() {
        return partyJoin;
    }

    public String getPartyAcceptOther() {
        return partyAcceptOther;
    }

    public String getPartyDeclineOther() {
        return partyDeclineOther;
    }

    public String getPartyLeaveOther() {
        return partyLeaveOther;
    }

    public String getPartyInviteOther() {
        return partyInviteOther;
    }

    public String getPartyKickOther() {
        return partyKickOther;
    }

    public String getPartyKickAll() {
        return partyKickAll;
    }

    public String getPartyJoinOther() {
        return partyJoinOther;
    }

    public String getErrorStillInParty() {
        return errorStillInParty;
    }

    public String getErrorNeedOwnParty() {
        return errorNeedOwnParty;
    }

    public String getErrorNoParty() {
        return errorNoParty;
    }

    public String getPartyDisband() {
        return partyDisband;
    }

    public String getErrorFriendSelf() {
        return errorFriendSelf;
    }

    public String getErrorInviteSelf() {
        return errorInviteSelf;
    }

    public String getGameInfo() {
        return gameInfo;
    }

    public String getNoTalkSpectate() {
        return noTalkSpectate;
    }

    public String getPartySend() {
        return partySend;
    }

    public String getPartySendOther() {
        return partySendOther;
    }
}
