package com.rast.gamecore;

import org.bukkit.entity.Player;

public abstract class GameInstance {

    private final GameWorld gameWorld;

    /**
     * Initiate the game instance.
     *
     * @param gameWorld the gameworld that this instances is paired to
     */
    public GameInstance(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    /**
     * Get the gameworld paired to this instance.
     *
     * @return the gameworld paired to this instance
     */
    public GameWorld getGameWorld() {
        return gameWorld;
    }

    public abstract void addPlayer(Player player);
    public abstract void removePlayer(Player player);
}
