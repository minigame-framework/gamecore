package com.rast.gamecore;

public enum GameStatus {
    RUNNING_CLOSED, RUNNING_OPEN, WAITING_OPEN, WAITING_CLOSED, SHUTDOWN
}
