package com.rast.gamecore.events;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameMaster;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.ArrayList;
import java.util.List;

public class ChatManager implements Listener {

    private final String globalFormat = GameCore.getSettings().getGlobalChatFormat();
    private final GameMaster gameMaster = GameCore.getGameMaster();

    // GameCore provides a built in chat formatter and grouper that can be configured int he plugin's config
    @EventHandler
    public void OnPlayerChat(AsyncPlayerChatEvent e) {
        e.setCancelled(true);

        Player player = e.getPlayer();
        Game game = gameMaster.getPlayerGame(player);

        if (game == null && !GameCore.getSettings().doSpectatorChat()) {
            player.sendMessage(GameCore.getLocale().getNoTalkSpectate());
            return;
        }

        String gst = globalFormat;
        String lst = globalFormat;

        if (game != null) {
            lst = game.getChatFormat(player);
        }

        // Fill the placeholders in the format for the local chat
        if (game != null) {
            lst = lst.replace("%game%", game.getName());
        } else {
            lst = lst.replace("%game%", "Spectating");
        }
        lst = lst.replace("%username%", player.getDisplayName());
        lst = lst.replace("%message%", e.getMessage());

        // Fill the placeholders in the format for the global chat
        if (GameCore.getSettings().doGlobalChat()) {
            if (game != null) {
                gst = gst.replace("%game%", game.getName());
            } else {
                gst = gst.replace("%game%", "Spectating");
            }
            gst = gst.replace("%username%", player.getDisplayName());
            gst = gst.replace("%message%", e.getMessage());
        }

        // Separate the players in the same world as the current player and
        List<Player> gamePlayers = new ArrayList<>();

        // Get the game's chat format for this player if the game exists
        if (game != null) {
            gamePlayers = player.getWorld().getPlayers();
        }

        // Get the server's players and remove all of the game players
        List<Player> onlinePlayers = new ArrayList<>(Bukkit.getServer().getOnlinePlayers());
        onlinePlayers.removeAll(gamePlayers);

        // Send the local message to all the players in the world
        for (Player recipient : gamePlayers) {
            recipient.sendMessage(lst);
        }

        // Send the global message to the rest of the people in the server if enabled
        if (GameCore.getSettings().doGlobalChat()) {
            for (Player recipient : onlinePlayers) {
                recipient.sendMessage(gst);
            }
        }
    }
}
