package com.rast.gamecore.events;

import com.rast.gamecore.GameCore;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {

    // We want to send the player to a the lobby game when they join so they are not in a 'limbo'
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame()).sendPlayer(player);
        GameCore.getFriendManager().fetchUsersFriends(player);
        GameCore.getScoreManager().fetchUserScores(player);

        String st = GameCore.getLocale().getWelcomeMessage();

        st = st.replace("%username%", player.getDisplayName());

        player.sendMessage(st);
    }

}
