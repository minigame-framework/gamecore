package com.rast.gamecore.events;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.party.Party;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuit implements Listener {

    // Tell the game that the player is in to remove the player
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        if (GameCore.getGameMaster().getPlayerGame(player) != null) {
            GameCore.getGameMaster().getPlayerGame(player).pullPlayer(player);
            GameCore.getFriendManager().removeUser(player);
            GameCore.getScoreManager().removePlayer(player);
            Party party = GameCore.getPartyManager().getParty(player);
            if (party != null) {
                if (party.isHost(player)) {
                    party.disbandParty();
                } else {
                    party.removeMember(player);
                }
            }
        }
    }
}
