package com.rast.gamecore.events;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameCore;
import com.rast.gamecore.KitsManager;
import com.rast.gamecore.util.ColorText;
import com.rast.gamecore.util.Kits;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;

public class KitMenuClick implements Listener {

    private final KitsManager kitsManager = GameCore.getKitsManager();

    // Detect when a player tries to open a kit menu
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Game playerGame = GameCore.getGameMaster().getPlayerGame(event.getPlayer());
        if (playerGame != null && event.getItem() != null) {
            Kits playerKits = kitsManager.getKits(playerGame);
            if (playerKits != null && event.getItem().equals(kitsManager.getKits(playerGame).getKitMenuItem())) {

                // generate the kit menu, open it for the player, and add it to the kit manager
                // order is important because opening the inventory closes the other one that will remove the saved inventory
                Inventory newInventory = kitsManager.getKits(playerGame).generateKitMenuInventory(event.getPlayer(), kitsManager.getKits(playerGame).getPlayerKit(event.getPlayer()));
                event.getPlayer().openInventory(newInventory);
                kitsManager.addPlayerMenu(event.getPlayer(), newInventory);

                event.setCancelled(true);
            }
        }
    }

    // Remove the player menu to prevent memory problems
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        kitsManager.removePlayerMenu((Player) event.getPlayer());
    }

    // Handle player interaction with the kit menu
    @EventHandler
    public void onInventoryInteract(InventoryClickEvent event) {

        if (event.getWhoClicked() instanceof Player) {

            Player player = (Player) event.getWhoClicked();
            Game playerGame = GameCore.getGameMaster().getPlayerGame(player);

            // Ensure that the game and the clicked inventory exist
            if (event.getClickedInventory() != null && playerGame != null) {

                // See if this is the player kit menu
                if (kitsManager.isPlayerMenu(player, event.getClickedInventory())) {

                    ItemStack item = event.getCurrentItem();

                    // We don't want to do anything if they clicked on air
                    if (item != null && !item.getType().equals(Material.AIR)) {

                        // Get the un-formatted name of the kit selected
                        String cleanName = null;
                        if (item.hasItemMeta()) {
                            cleanName = ColorText.BleachText(Objects.requireNonNull(event.getCurrentItem().getItemMeta()).getDisplayName());
                        }

                        // If they click the exit button, close the inventory
                        if (event.getSlot() == 53) {
                            player.closeInventory();
                            event.setCancelled(true);
                            kitsManager.removePlayerMenu(player);
                            return;

                        // If they click the random kit button, set the kit null
                        } else if (event.getSlot() == 49) {
                            kitsManager.getKits(playerGame).setPlayerKit(player, null);
                            Inventory newInventory = kitsManager.getKits(playerGame).generateKitMenuInventory(player, null);
                            Bukkit.getServer().getScheduler().runTask(GameCore.getPlugin(), () -> {
                                player.openInventory(newInventory);
                                kitsManager.addPlayerMenu(player, newInventory);
                            });

                        // Set the kit that is selected
                        } else if (event.getSlot() < 45) {
                            kitsManager.getKits(playerGame).setPlayerKit(player, cleanName);
                            Inventory newInventory = kitsManager.getKits(playerGame).generateKitMenuInventory(player, cleanName);
                            Bukkit.getServer().getScheduler().runTask(GameCore.getPlugin(), () -> {
                                player.openInventory(newInventory);
                                kitsManager.addPlayerMenu(player, newInventory);
                            });
                        }
                    }
                    event.setCancelled(true);
                }
            }
        }
    }
}
