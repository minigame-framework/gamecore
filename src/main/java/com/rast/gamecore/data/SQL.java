package com.rast.gamecore.data;

import com.rast.gamecore.GameCore;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQL {

    private final String host, database, username, password;
    private final int port;

    /**
     * SQL is used to store data to an external data center
     * leaving the parameters empty will create a sql object with settings derived from GameCore's config
     *
     * @param host     the ip of the sql server
     * @param port     the port for the sql server
     * @param database the name of the database to access
     * @param username the username to access the database
     * @param password the password to access the database
     */
    public SQL(String host, int port, String database, String username, String password) {
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    /**
     * SQL is used to store data to an external data center
     * leaving the parameters empty will create a sql object with settings derived from GameCore's config
     */
    public SQL() {
        this.host = GameCore.getSettings().getSqlHost();
        this.port = GameCore.getSettings().getSqlPort();
        this.database = GameCore.getSettings().getSqlDatabase();
        this.username = GameCore.getSettings().getSqlUsername();
        this.password = GameCore.getSettings().getSqlPassword();
    }

    /**
     * Opens a new SQL connection to the data server (Async compatible)
     * From here data can be sent and retrieved from the server
     * Remember to close any connection made when the connection is no longer needed
     *
     * @return a new Connection from the SQL server
     * @throws SQLException           The SQL server was not able to connect correctly
     * @throws ClassNotFoundException The class "com.mysql.jdbc.Driver" was not found. Install MySql JConnector
     */
    public Connection openConnection() throws SQLException, ClassNotFoundException {
        synchronized (GameCore.getPlugin()) {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?useSSL=false", this.username, this.password);
        }
    }
}
