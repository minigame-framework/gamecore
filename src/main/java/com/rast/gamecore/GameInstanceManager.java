package com.rast.gamecore;

import com.rast.gamecore.util.DirectoryTools;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class GameInstanceManager {

    // variables
    private final Set<GameInstance> gameInstances = new HashSet<>();
    private final Game game;

    /**
     * The game instance manager is used to help you manage all your game instances.
     *
     * @param game the game parenting this instance
     */
    public GameInstanceManager(Game game) {
        this.game = game;
        game.setGameInstanceManager(this);
    }

    /**
     * Get the game instance that this player is in.
     *
     * @param player the player to find the instance of
     * @return the instance the player is in, null if no instance was found
     */
    public GameInstance getInstanceFromPlayer(Player player) {
        for (GameInstance instance : gameInstances) {
            if (instance.getGameWorld().getBukkitWorld().getPlayers().contains(player)) {
                return instance;
            }
        }
        return null;
    }

    /**
     * Get all the game instances.
     *
     * @return a set of game instances
     */
    public Set<GameInstance> getGameInstances() {
        return gameInstances;
    }

    /**
     * Get an instance from a given world.
     *
     * @param world the world to find the instance of
     * @return the instance the world is on, null if no instance was found
     */
    public GameInstance getInstanceFromWorld(World world) {
        for (GameInstance instance : gameInstances) {
            if (instance.getGameWorld().getBukkitWorld().equals(world)) {
                return instance;
            }
        }
        return null;
    }

    /**
     * Determine if an instance exists.
     *
     * @param instance the instance to check
     * @return true if the instance exists, else false
     */
    public boolean instanceExists(GameInstance instance) {
        return gameInstances.contains(instance);
    }

    /**
     * Add an instance to the instance manager.
     *
     * @param instance the instance to add
     * @return true if the instance was added
     * this method will return false if the instance already existed or the world was invalid
     */
    public boolean addInstance(GameInstance instance) {
        if (instance.getGameWorld().isValid() && !instanceExists(instance)) {
            instance.getGameWorld().setStatus(GameStatus.WAITING_OPEN);
            gameInstances.add(instance);
            return false;
        }
        return true;
    }

    /**
     * Remove an instance from the instance manager.
     *
     * @param instance the instance to remove
     */
    public void removeInstance(GameInstance instance) {
        gameInstances.remove(instance);
    }

    /**
     * This method will remove the player from any instance in the game and add it to the specified instance.
     *
     * @param player   the player to add to a instance
     * @param instance the instance the player is being added to
     */
    public void addPlayerToInstance(Player player, GameInstance instance) {
        wipePlayer(player);
        instance.addPlayer(player);
    }

    /**
     * Remove the player from all instances in the game.
     *
     * @param player player to wipe
     */
    public void wipePlayer(Player player) {
        for (GameInstance instance : gameInstances) {
            instance.removePlayer(player);
        }
    }

    /**
     * Remove all instances from the game. This includes deleting the world.
     */
    public void purgeInstances() {
        Game lobby = GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame());
        for (GameInstance instance : gameInstances) {
            World world = instance.getGameWorld().getBukkitWorld();
            File worldFolder = world.getWorldFolder();
            for (Player player : world.getPlayers()) {
                game.pullPlayer(player);
                lobby.sendPlayer(player);
            }
            Bukkit.unloadWorld(world, false);
            DirectoryTools.deleteFolder(worldFolder);
        }
        gameInstances.clear();
    }

    /**
     * Remove an instance from the game. This includes deleting the world.
     *
     * @param instance the instance to remove
     */
    public void purgeInstance(GameInstance instance) {
        Game lobby = GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame());
        World world = instance.getGameWorld().getBukkitWorld();
        File worldFolder = world.getWorldFolder();
        for (Player player : world.getPlayers()) {
            game.pullPlayer(player);
            lobby.sendPlayer(player);
        }
        Bukkit.unloadWorld(world, false);
        DirectoryTools.deleteFolder(worldFolder);
        gameInstances.remove(instance);
    }
}
