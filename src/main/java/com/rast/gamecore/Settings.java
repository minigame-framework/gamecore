package com.rast.gamecore;

import com.rast.gamecore.util.ColorText;
import com.rast.gamecore.util.ConfigSettings;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.Objects;

public class Settings extends ConfigSettings {

    private boolean globalChat, spectatorChat, useSQL, friendsEnabled;
    private String globalChatFormat, defaultGame, sqlHost, sqlDatabase, sqlUsername, sqlPassword;
    private World defaultWorld;
    private int sqlPort, friendCacheThreshold, maxFriends;

    /**
     * All the settings for the GameCore or any other plugin that wants to read them
     */
    public Settings() {
        reload();
    }

    @Override
    public void reload() {
        // Get the plugin, reload config, and get the config
        GameCore plugin = GameCore.getPlugin();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        // Grab all the settings
        globalChat = config.getBoolean("global-chat");
        spectatorChat = config.getBoolean("spectator-chat");
        globalChatFormat = ColorText.TranslateChat(config.getString("global-chat-format"));
        defaultWorld = plugin.getServer().getWorld(Objects.requireNonNull(config.getString("default-world")));
        defaultGame = config.getString("default-game");
        useSQL = config.getBoolean("use-sql-database");
        sqlHost = config.getString("host");
        sqlDatabase = config.getString("database");
        sqlUsername = config.getString("username");
        sqlPassword = config.getString("password");
        sqlPort = config.getInt("port");
        friendsEnabled = config.getBoolean("friends-enabled");
        friendCacheThreshold = config.getInt("friend-cache-threshold");
        maxFriends = config.getInt("max-friends");
    }

    public boolean doGlobalChat() {
        return globalChat;
    }

    public String getGlobalChatFormat() {
        return globalChatFormat;
    }

    public World getDefaultWorld() {
        return defaultWorld;
    }

    public String getDefaultGame() {
        return defaultGame;
    }

    public boolean useSQL() {
        return useSQL;
    }

    public String getSqlHost() {
        return sqlHost;
    }

    public String getSqlDatabase() {
        return sqlDatabase;
    }

    public String getSqlUsername() {
        return sqlUsername;
    }

    public String getSqlPassword() {
        return sqlPassword;
    }

    public int getSqlPort() {
        return sqlPort;
    }

    public int getFriendCacheThreshold() {
        return friendCacheThreshold;
    }

    public int getMaxFriends() {
        return maxFriends;
    }

    public boolean doSpectatorChat() {
        return spectatorChat;
    }

    public boolean isFriendsEnabled() {
        return friendsEnabled;
    }
}
