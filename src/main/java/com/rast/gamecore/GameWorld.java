package com.rast.gamecore;

import com.rast.gamecore.util.DirectoryTools;
import org.bukkit.GameRule;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class GameWorld {

    private final World world;
    private final boolean validWorld;
    private int playerCount;
    private final int maxPlayers;
    private String name;
    private GameStatus status;
    private final Game game;
    private final String map;

    /**
     * Create a pebble world with advanced functionality
     * this will either create a world or return a world that already exists
     *
     * @param plugin     the plugin
     * @param game       the game to run on this world
     * @param map        the map to put on this world
     * @param maxPlayers the max players for this gameworld
     * @throws IOException Throws an IOException when the world template file did not clone
     */
    public GameWorld(Game game, String map, int maxPlayers, JavaPlugin plugin) throws IOException {
        this.game = game;
        this.map = map;
        this.maxPlayers = maxPlayers;
        name = "GAMECORE-" + UUID.randomUUID().toString();

        // Get world folders
        File worldTemplateFolder = new File(plugin.getDataFolder().getAbsoluteFile() + "/maps/" + map + "/");
        File worldFolder = new File(plugin.getDataFolder().getAbsoluteFile().getParentFile().getParentFile() + "/" + name + "/");

        // If the plugin is not enabled the server might not exist
        if (plugin.isEnabled()) {
            plugin.getServer().unloadWorld(name, false);
        }

        // Clone the template world folder
        try {
            DirectoryTools.copyFolder(worldTemplateFolder, worldFolder);
        } catch (IOException e) {
            plugin.getLogger().severe("Creation of world '" + name + "' failed! The plugin '" + plugin.getName() + "' was trying to make the world.");
            world = null;
            validWorld = false;
            return;
        }

        // Reset the world uid
        File uid = new File(worldFolder.getPath() + "/uid.dat");
        if (uid.exists()) {
            if (!uid.delete()) {
                plugin.getLogger().severe("Creation of world '" + name + "' failed! The plugin '" + plugin.getName() + "' was trying to make the world.");
                world = null;
                validWorld = false;
                return;
            }
        }

        // Make the world
        WorldCreator wc = new WorldCreator(name);
        world = wc.createWorld();

        if (world == null) {
            plugin.getLogger().severe("Creation of world '" + name + "' failed! The plugin '" + plugin.getName() + "' was trying to make the world.");
            validWorld = false;
            return;
        }

        // The spawn does not need to stay in memory
        world.setKeepSpawnInMemory(false);

        // Do not autosave because the world will be deleted when server shuts down anyways
        world.setAutoSave(false);

        // Set Default GameRules
        world.setGameRule(GameRule.DO_IMMEDIATE_RESPAWN, true);
        world.setGameRule(GameRule.DO_ENTITY_DROPS, false);
        world.setGameRule(GameRule.DO_MOB_LOOT, false);
        world.setGameRule(GameRule.DO_MOB_SPAWNING, false);
        world.setGameRule(GameRule.DO_FIRE_TICK, false);
        world.setGameRule(GameRule.MOB_GRIEFING, false);
        world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
        world.setGameRule(GameRule.DO_TILE_DROPS, true);
        world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);

        validWorld = true;
    }

    /**
     * Get the bukket world
     *
     * @return the bukket world
     */
    public World getBukkitWorld() {
        return world;
    }

    /**
     * See if the world is valid
     *
     * @return true if world is valid and false if it is bad/failed to create properly
     */
    public boolean isValid() {
        return validWorld;
    }

    /**
     * Get the game map
     *
     * @return the world game type
     */
    public String getGameType() {
        return map;
    }

    /**
     * Get the name of the world
     *
     * @return the world name
     */
    public String getName() {
        return name;
    }

    /**
     * Add a player to the player count (add players if they are joining the game)
     */
    public void addPlayer() {
        playerCount++;
    }

    /**
     * Remove a player from the player count (remove players if they are leaving the game)
     */
    public void removePlayer() {
        playerCount--;
    }

    /**
     * Gets the player count from this game world
     *
     * @return the player count (This is not the total players in the world, only the total that are in the game)
     */
    public int getPlayerCount() {
        return playerCount;
    }

    /**
     * Sets the player count for the gameWorld
     *
     * @param playerCount new player count
     */
    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    /**
     * Gets the max player count from this game world
     *
     * @return the max player count (Does not directly effect the actual player count)
     */
    public int getMaxPlayerCount() {
        return maxPlayers;
    }

    /**
     * Get the game's status
     *
     * @return the game's status
     */
    public GameStatus getStatus() {
        return status;
    }

    /**
     * Set the game's status
     *
     * @param status the game's status
     */
    public void setStatus(GameStatus status) {
        this.status = status;
    }

    /**
     * Get the game that is running on this world
     *
     * @return the Game object
     */
    public Game getGame() {
        return game;
    }

    /**
     * Get the map that this world is instanced after
     *
     * @return the map of the game
     */
    public String getMap() {
        return map;
    }

}
