package com.rast.gamecore.subcommand;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameWorld;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class GameInfoSubcommand extends Subcommand {

    public GameInfoSubcommand(GameCore plugin) {
        super(plugin);
    }


    @Override
    public String getHelp() {
        return "Get info about the current game";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " gameinfo";
    }

    @Override
    public String getPermission() {
        return "gamecore.debug";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        Game game = GameCore.getGameMaster().getPlayerGame(player);

        player.sendMessage(GameCore.getLocale().getSystemPrefix() + GameCore.getLocale().getGameInfo() + ChatColor.GRAY + ":");
        if (game != null) {
            player.sendMessage(ChatColor.GOLD + "Game: " + ChatColor.WHITE + game.getName());
            for (GameWorld gameWorld : game.getGameSet()) {
                if (gameWorld.getBukkitWorld().getPlayers().contains(player)) {
                    player.sendMessage(ChatColor.GOLD + "GameWorld: " + ChatColor.WHITE + gameWorld.getName());
                    player.sendMessage(ChatColor.GOLD + "Map: " + ChatColor.WHITE + gameWorld.getMap());
                    player.sendMessage(ChatColor.GOLD + "GameWorld Status: " + ChatColor.WHITE + gameWorld.getStatus().toString());
                    player.sendMessage(ChatColor.GOLD + "GameWorld Player Count: " + ChatColor.WHITE + gameWorld.getPlayerCount());
                    player.sendMessage(ChatColor.GOLD + "GameWorld Max Players: " + ChatColor.WHITE + gameWorld.getMaxPlayerCount());
                }
            }
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

}
