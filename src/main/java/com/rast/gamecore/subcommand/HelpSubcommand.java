package com.rast.gamecore.subcommand;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class HelpSubcommand extends Subcommand {


    public HelpSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Gives help for the plugin";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " help";
    }

    @Override
    public String getPermission() {
        return "gamecore.admin";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        player.sendMessage(" ");
        player.sendMessage(ChatColor.GRAY + "Help for " + GameCore.getLocale().getSystemPrefix() + ChatColor.GRAY + ":");

        for (Subcommand subcommand : GameCore.getSubCommands().values()) {
            if (player.hasPermission(subcommand.getPermission()))
                player.sendMessage(ChatColor.GOLD + subcommand.getUsage(label) + ": " + ChatColor.WHITE + subcommand.getHelp());
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
