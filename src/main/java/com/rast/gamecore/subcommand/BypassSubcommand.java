package com.rast.gamecore.subcommand;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class BypassSubcommand extends Subcommand {

    public BypassSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Bypass the games to an extent";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " bypass";
    }

    @Override
    public String getPermission() {
        return "gamecore.admin";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        if (args.length == 2 && args[1].equals("false")) {
            if (GameCore.getGameMaster().getPlayerGame(player) != null) {
                GameCore.getGameMaster().getPlayerGame(player).pullPlayer(player);
            }
            GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame()).sendPlayer(player);
            player.sendMessage(GameCore.getLocale().getSystemPrefix() + ChatColor.GREEN + "You are no longer bypassing games!");
            return true;
        }
        for (Game game : GameCore.getGameMaster().getGames()) {
            game.pullPlayer(player);
        }
        player.sendMessage(GameCore.getLocale().getSystemPrefix() + ChatColor.GREEN + "You are now bypassing games!");
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 2) {
            List<String> tabList = new ArrayList<>();
            tabList.add("true");
            tabList.add("false");
            return tabList;
        }
        return null;
    }

}
