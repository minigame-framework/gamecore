package com.rast.gamecore.subcommand;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class GameListSubcommand extends Subcommand {

    public GameListSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Get a list of registered games";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " list";
    }

    @Override
    public String getPermission() {
        return "gamecore.admin";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(GameCore.getLocale().getSystemPrefix() + ChatColor.GOLD + "Games currently registered:");
        for (Game game : GameCore.getGameMaster().getGames()) {
            sender.sendMessage(ChatColor.DARK_GRAY + " - " + ChatColor.YELLOW + game.getName());
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 2) {
            List<String> tabList = new ArrayList<>();
            tabList.add("true");
            tabList.add("false");
            return tabList;
        }
        return null;
    }

}