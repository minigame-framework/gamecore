package com.rast.gamecore.party;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class PartyInviteSubcommand extends Subcommand {


    public PartyInviteSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Invites a player to your party";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " invite";
    }

    @Override
    public String getPermission() {
        return "gamecore.party";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;
        Party party = GameCore.getPartyManager().getParty(player);

        if (party != null && party.isHost(player)) {
            if (args.length < 2) {
                player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyNoPlayerSpecified());
            } else {
                Player receiver = Bukkit.getPlayer(args[1]);
                if (receiver != null) {
                    GameCore.getPartyManager().sendPartyInvite(player, receiver);
                } else {
                    player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyUserNotFound());
                }
            }
        } else {
            player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getErrorNeedOwnParty());
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        List<String> tabList = new ArrayList<>();
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            tabList.add(player.getName());
        }
        return tabList;
    }
}
