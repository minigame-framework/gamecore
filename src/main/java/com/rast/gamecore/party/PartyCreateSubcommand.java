package com.rast.gamecore.party;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class PartyCreateSubcommand extends Subcommand {


    public PartyCreateSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Creates a new party";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " create";
    }

    @Override
    public String getPermission() {
        return "gamecore.party.create";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;
        Party party = GameCore.getPartyManager().getParty(player);

        if (party != null) {
            player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getErrorStillInParty());
            return true;
        }
        GameCore.getPartyManager().createParty(player);
        player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyCreate());
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
