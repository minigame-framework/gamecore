package com.rast.gamecore.party;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameWorld;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class PartyManager {

    private final HashMap<Player, Party> parties = new HashMap<>();

    private final HashMap<Player, Player> partyInvitations = new HashMap<>();

    /**
     * Create a party which is then added to the PartyManager
     * If a the host already owns a party, this will return the old party
     *
     * @param host the host of the party
     * @return the party the that host owns
     */
    public Party createParty(Player host) {
        Party party = getParty(host);
        if (party != null) {
            if (party.isHost(host)) {
                return party;
            }
            party.removeMember(host);
        }

        party = new Party(host);
        parties.put(host, party);
        return party;
    }

    /**
     * Delete a party
     *
     * @param host the host of the party
     */
    public void deleteParty(Player host) {
        if (parties.get(host) != null) {
            parties.get(host).disbandParty();
            parties.remove(host);
        }
    }

    /**
     * Get a party which the player is apart of
     *
     * @param player the player to get the party of
     * @return the party the player is apart of or null if no party is found
     */
    public Party getParty(Player player) {
        for (Map.Entry<Player, Party> partyEntry : parties.entrySet()) {
            if (partyEntry.getValue().contains(player)) {
                return partyEntry.getValue();
            }
        }
        return null;
    }

    /**
     * Send a party invitation form one player to another to later be accepted
     *
     * @param sender   the player who is sending the party invite
     * @param receiver the player who is receiving the party invite
     */
    public void sendPartyInvite(Player sender, Player receiver) {
        if (sender.equals(receiver)) {
            sender.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getErrorInviteSelf());
            return;
        }
        if (getParty(receiver) != null && getParty(receiver).equals(getParty(sender))) {
            return;
        }
        sender.playSound(sender.getLocation(), Sound.ENTITY_VILLAGER_TRADE, SoundCategory.MASTER, Float.MAX_VALUE, 1.5f);
        receiver.playSound(receiver.getLocation(), Sound.ENTITY_VILLAGER_TRADE, SoundCategory.MASTER, Float.MAX_VALUE, 1.5f);
        sender.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyInvite().replace("%user%", receiver.getName()));
        receiver.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyInviteOther().replace("%user%", sender.getName()));
        partyInvitations.put(receiver, sender);
    }

    /**
     * Accept the latest party invitation
     *
     * @param player the player accepting the invitation
     */
    public void acceptPartyInvite(Player player) {
        Party newParty = getParty(partyInvitations.get(player));
        Party oldParty = getParty(player);
        if (oldParty != null) {
            player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getErrorStillInParty());
            return;
        }

        if (newParty != null) {
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, SoundCategory.MASTER, Float.MAX_VALUE, 1.5f);
            newParty.getHost().playSound(newParty.getHost().getLocation(), Sound.ENTITY_VILLAGER_YES, SoundCategory.MASTER, Float.MAX_VALUE, 1.5f);
            newParty.getHost().sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyAcceptOther().replace("%user%", player.getName()));
            newParty.addMember(player);
            player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyAccept().replace("%user%", newParty.getHost().getName()));
            partyInvitations.remove(player);
        }
    }

    /**
     * Deny the latest party invite
     *
     * @param player the player denying the invite
     */
    public void denyPartyInvite(Player player) {
        Player host = partyInvitations.get(player);
        if (host != null) {
            player.playSound(player.getLocation(), Sound.ENTITY_VINDICATOR_AMBIENT, SoundCategory.MASTER, Float.MAX_VALUE, 2);
            host.playSound(host.getLocation(), Sound.ENTITY_VINDICATOR_AMBIENT, SoundCategory.MASTER, Float.MAX_VALUE, 2);
            player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyDecline().replace("%user%", host.getName()));
            host.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyDeclineOther().replace("%user%", player.getName()));
            partyInvitations.remove(player);
        }
    }

    /**
     * Send the members of your party to the host's current match
     *
     * @param player the person sending the party (must be host)
     */
    public void sendParty(Player player) {
        Party party = GameCore.getPartyManager().getParty(player);

        if (party == null || !party.isHost(player)) {
            player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getErrorNeedOwnParty());
            return;
        }
        Game game = GameCore.getGameMaster().getPlayerGame(player);
        if (game == null) {
            return;
        }
        GameWorld gameWorld = game.getGameWorld(player.getWorld());
        if (gameWorld == null) {
            return;
        }

        player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartySend().replace("%game%", game.getName()));
        for (Player member : party.getMembers()) {
            Game playerGame = GameCore.getGameMaster().getPlayerGame(member);
            if (playerGame != null) {
                if (playerGame.equals(game)) {
                    return;
                }
                playerGame.pullPlayer(member);
            }
            game.sendPlayer(member, gameWorld);
            member.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartySendOther().replace("%game%", game.getName()));
        }
    }


}
