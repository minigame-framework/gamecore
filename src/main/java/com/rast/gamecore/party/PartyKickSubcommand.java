package com.rast.gamecore.party;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class PartyKickSubcommand extends Subcommand {


    public PartyKickSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Kicks a member from the party";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " kick";
    }

    @Override
    public String getPermission() {
        return "gamecore.party";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;
        Party party = GameCore.getPartyManager().getParty(player);

        if (party != null && party.isHost(player)) {
            if (args.length < 2) {
                player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyNoPlayerSpecified());
            } else {
                Player receiver = Bukkit.getPlayer(args[1]);
                if (receiver != null) {
                    if (party.getMembers().contains(receiver))
                        party.kickMember(receiver);
                } else {
                    player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyUserNotFound());
                }
            }
        } else {
            player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getErrorNeedOwnParty());
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        List<String> tabList = new ArrayList<>();
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Party party = GameCore.getPartyManager().getParty(player);
            if (party != null && party.isHost(player)) {
                for (Player member : GameCore.getPartyManager().getParty(player).getMembers()) {
                    tabList.add(member.getName());
                }
            }
        }
        return tabList;
    }
}
