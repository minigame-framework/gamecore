package com.rast.gamecore.party;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameWorld;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class Party {
    private final Player host;
    private final HashSet<Player> members = new HashSet<>();
    private Game game;
    private GameWorld gameWorld;

    /**
     * Create a new party
     * Parties are used to keep players together in games
     *
     * @param host the host of the party
     */
    public Party(Player host) {
        this.host = host;
    }

    /**
     * Add a member to the party
     *
     * @param player the new member
     */
    public void addMember(Player player) {
        if (!members.contains(player)) {
            player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyJoin().replace("%user%", host.getName()));
            for (Player mem : members) {
                mem.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyJoinOther().replace("%user%", player.getName()));
            }
            host.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyJoinOther().replace("%user%", player.getName()));
            members.add(player);
            checkMembers();
        }
    }

    /**
     * Remove a member from the lobby
     *
     * @param player the player to remove
     */
    public void removeMember(Player player) {
        if (members.contains(player)) {
            members.remove(player);
            player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyLeave().replace("%user%", host.getName()));
            for (Player mem : members) {
                mem.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyLeaveOther().replace("%user%", player.getName()));
            }
            host.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyLeaveOther().replace("%user%", player.getName()));
            checkMembers();
        }
    }

    /**
     * Get the game that the party is playing
     *
     * @return the party's game
     */
    public Game getGame() {
        return game;
    }

    /**
     * Set the game that the party is playing
     *
     * @param game the party's game
     */
    public void setGame(Game game) {
        this.game = game;
    }

    /**
     * Get the gameworld that the party is in
     *
     * @return the gameworld the party is in
     */
    public GameWorld getGameWorld() {
        return gameWorld;
    }

    /**
     * Set the gameworld that the party is in
     * This is the gameworld that the host is in
     *
     * @param gameWorld the gameworld the party is in
     */
    public void setGameWorld(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    /**
     * Get the host of the party
     *
     * @return the party's host
     */
    public Player getHost() {
        return host;
    }

    /**
     * Get the members of the party
     * This does not include the host
     *
     * @return the party members
     */
    public HashSet<Player> getMembers() {
        checkMembers();
        return members;
    }

    /**
     * Get all the players in the party
     * This is all the members and the host
     *
     * @return the players in the party
     */
    public HashSet<Player> getAll() {
        checkMembers();
        HashSet<Player> allUsers = new HashSet<>(members);
        allUsers.add(host);
        return allUsers;
    }

    /**
     * Get the size of the party as a count of players
     * This is the amount of members plus the host
     *
     * @return the size of the party
     */
    public int getSize() {
        return members.size() + 1;
    }

    /**
     * Test if the party contains a player
     *
     * @param player the player to look for
     * @return true if the party has this player, else false
     */
    public boolean contains(Player player) {
        return (isHost(player) || isMember(player));
    }

    /**
     * Test if the player is the host of the party
     *
     * @param player the player to test
     * @return true if player is the host, else false
     */
    public boolean isHost(Player player) {
        return (host.equals(player));
    }

    /**
     * Test if the player is a member
     *
     * @param player the player to test
     * @return true if player is a member, else false
     */
    public boolean isMember(Player player) {
        return (members.contains(player));
    }

    /**
     * Remove all players from the party
     * Host is informed that the party has been disbanded
     */
    public void disbandParty() {
        host.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyDisband());
        HashSet<Player> membersToRemove= new HashSet<>(members);
        for (Player member : membersToRemove) {
            member.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyDisband());
            removeMember(member);
        }
    }

    /**
     * Send the members of the party to a specific game and world
     * This is intended to send the party members to the world the host is in
     *
     * @param game      the game to send to
     * @param gameWorld the world to send to
     */
    public void sendParty(Game game, GameWorld gameWorld) {
        checkMembers();
        for (Player member : members) {
            game.sendPlayer(member);
        }
    }

    /**
     * Kick a member from the party displaying it to the party
     * Works just as remove, but humiliates the kicked person by telling all in the party they were kicked
     *
     * @param member the member to kick
     */
    public void kickMember(Player member) {
        if (members.contains(member)) {
            members.remove(member);
            member.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyKickOther().replace("%user%", host.getName()));
            host.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyKick().replace("%user%", member.getName()));
            for (Player mem : members) {
                mem.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyKickAll().replace("%user%", member.getName()));
            }
        }
        checkMembers();
    }

    /**
     * Look for players no longer online and remove them
     */
    private void checkMembers() {
        for (Player member : members) {
            if (!member.isOnline()) {
                removeMember(member);
            }
        }
    }
}
