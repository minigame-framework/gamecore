package com.rast.gamecore.party;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class PartyLeaveSubcommand extends Subcommand {


    public PartyLeaveSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Leave your current party";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " leave";
    }

    @Override
    public String getPermission() {
        return "gamecore.party";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;
        Party party = GameCore.getPartyManager().getParty(player);

        if (party != null) {
            if (party.contains(player)) {
                if (party.isHost(player)) {
                    player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyLeave().replace("%user%", party.getHost().getName()));
                    GameCore.getPartyManager().deleteParty(player);
                } else {
                    party.removeMember(player);
                }
            } else {
                player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getErrorNoParty());
            }
        } else {
            player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getErrorNoParty());
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
