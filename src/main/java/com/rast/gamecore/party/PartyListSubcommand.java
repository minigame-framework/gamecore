package com.rast.gamecore.party;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Iterator;
import java.util.List;

public class PartyListSubcommand extends Subcommand {


    public PartyListSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "List the people in the party";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " list";
    }

    @Override
    public String getPermission() {
        return "gamecore.party";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        Party party = GameCore.getPartyManager().getParty(player);
        if (party == null) {
            player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getErrorNoParty());
            return true;
        }

        player.sendMessage(GameCore.getLocale().getPartyPrefix() + GameCore.getLocale().getPartyList());

        StringBuilder builder = new StringBuilder();
        Iterator<Player> iterator = party.getAll().iterator();
        while (iterator.hasNext()) {
            Player member = iterator.next();
            builder.append(ChatColor.GRAY).append(member.getName());
            if (iterator.hasNext()) {
                builder.append(ChatColor.DARK_GRAY).append(", ");
            }
            if (builder.length() > 20) {
                player.sendMessage(builder.toString());
                builder = new StringBuilder();
            }
        }
        String finalString  = builder.toString();
        if (!finalString.isEmpty()) {
            player.sendMessage(finalString);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
