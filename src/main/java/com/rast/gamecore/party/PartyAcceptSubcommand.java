package com.rast.gamecore.party;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class PartyAcceptSubcommand extends Subcommand {


    public PartyAcceptSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Accepts your most recent party invite";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " accept";
    }

    @Override
    public String getPermission() {
        return "gamecore.party";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        GameCore.getPartyManager().acceptPartyInvite(player);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
