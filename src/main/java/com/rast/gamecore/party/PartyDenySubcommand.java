package com.rast.gamecore.party;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class PartyDenySubcommand extends Subcommand {


    public PartyDenySubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Denies your most recent party invite";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " deny";
    }

    @Override
    public String getPermission() {
        return "gamecore.party";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        GameCore.getPartyManager().denyPartyInvite(player);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
