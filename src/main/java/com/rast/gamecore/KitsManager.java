package com.rast.gamecore;

import com.rast.gamecore.util.Kits;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;

public class KitsManager {

    private final HashMap<Player, Inventory> playerMenus = new HashMap<>();
    private final HashMap<Game, Kits> gameKits = new HashMap<>();

    /**
     * Register a kits class with the kits manager.
     * This will allow the kits manager to handle the kits menu.
     *
     * @param game the game the kits belong to
     * @param kits the kits class to register
     */
    public void registerKits(Game game, Kits kits) {
        gameKits.put(game, kits);
    }

    /**
     * Unregister a kits class from the kits manager.
     *
     * @param game the game the kits belong to
     */
    public void unregisterKits(Game game) {
        gameKits.remove(game);
    }

    /**
     * Got the kits class that was registered by the indicated game.
     *
     * @param game the game to get the kits class for
     * @return the kits class, null if no class was registered
     */
    public Kits getKits(Game game) {
        return gameKits.get(game);
    }

    /**
     * Add a player menu to be accessed later.
     * Used to validate an inventory's identity.
     *
     * @param player    the player that the inventory belongs to
     * @param inventory the inventory to store
     */
    public void addPlayerMenu(Player player, Inventory inventory) {
        playerMenus.put(player, inventory);
    }

    /**
     * Get the player menu associated with a player.
     * Used to validate an inventory's identity.
     *
     * @param player the player that the inventory belongs to
     * @return the inventory found associated with the player, null if none was found
     */
    public Inventory getPlayerMenu(Player player) {
        return playerMenus.get(player);
    }

    /**
     * Remove a player menu from the kits manager.
     * Used to prevent memory from getting out of hand.
     *
     * @param player the player that owns the inventory to remove
     */
    public void removePlayerMenu(Player player) {
        playerMenus.remove(player);
    }

    /**
     * Check to see if the inventory is the player's current kits menu.
     * This verifies the identity of the inventory accurately.
     *
     * @param player    the player to check to kit menu of
     * @param inventory the inventory being checked
     * @return true if the inventory is the player's kit menu, false if it is not the player's kit menu
     */
    public boolean isPlayerMenu(Player player, Inventory inventory) {
        return inventory.equals(getPlayerMenu(player));
    }

}
