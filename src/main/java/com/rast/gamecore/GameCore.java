package com.rast.gamecore;

import com.rast.gamecore.data.SQL;
import com.rast.gamecore.events.*;
import com.rast.gamecore.friends.*;
import com.rast.gamecore.party.*;
import com.rast.gamecore.scores.ScoreManager;
import com.rast.gamecore.standaloneCommands.LeaveCommand;
import com.rast.gamecore.subcommand.BypassSubcommand;
import com.rast.gamecore.subcommand.GameInfoSubcommand;
import com.rast.gamecore.subcommand.GameListSubcommand;
import com.rast.gamecore.subcommand.HelpSubcommand;
import com.rast.gamecore.util.*;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class GameCore extends JavaPlugin {

    private static GameCore plugin;
    private static GlobalRandom globalRandom;
    private static GameMaster gameMaster = new GameMaster();
    private static final KitsManager kitsManager = new KitsManager();
    private static Settings settings;
    private static Locale locale;
    private static final HashMap<String, Subcommand> subCommands = new HashMap<>();
    private static final HashMap<String, Subcommand> friendSubCommands = new HashMap<>();
    private static final HashMap<String, Subcommand> partySubCommands = new HashMap<>();
    private static FriendManager friendManager;
    private static ScoreManager scoreManager;
    private static PartyManager partyManager;
    private static SQL sql;

    /**
     * Gets the GameCore plugin
     *
     * @return the GameCore plugin
     */
    public static GameCore getPlugin() {
        return plugin;
    }

    /**
     * Get a hashmap of subcommands for GameCore
     *
     * @return game core subcommands in a hashmap
     */
    public static HashMap<String, Subcommand> getSubCommands() {
        return subCommands;
    }

    /**
     * Get a hashmap of friendSubcommands for GameCore
     *
     * @return game core friendSubcommands in a hashmap
     */
    public static HashMap<String, Subcommand> getFriendSubCommands() {
        return friendSubCommands;
    }

    /**
     * Get a hashmap of partySubcommands for GameCore
     *
     * @return game core partySubcommands in a hashmap
     */
    public static HashMap<String, Subcommand> getPartySubCommands() {
        return partySubCommands;
    }

    /**
     * Gets the global random that is used by the games in GameCore for random number generation.
     *
     * @return the global random object
     */
    public static GlobalRandom getGlobalRandom() {
        return globalRandom;
    }

    /**
     * Gets the game master that is used for registering games and managing groups
     *
     * @return the game manager
     */
    public static GameMaster getGameMaster() {
        return gameMaster;
    }

    /**
     * Gets the kits manager that is used for handling kit menus
     *
     * @return the kits manager
     */
    public static KitsManager getKitsManager() {
        return kitsManager;
    }

    /**
     * Gets the settings for GameCore
     *
     * @return the settings
     */
    public static Settings getSettings() {
        return settings;
    }

    /**
     * Gets the locale for GameCore
     *
     * @return the locale
     */
    public static Locale getLocale() {
        return locale;
    }

    /**
     * Gets the friend manager
     *
     * @return the friend manager
     */
    public static FriendManager getFriendManager() {
        return friendManager;
    }

    /**
     * Gets the score manager
     *
     * @return the score manager
     */
    public static ScoreManager getScoreManager() {
        return scoreManager;
    }

    /**
     * Gets the party manager
     *
     * @return the party manager
     */
    public static PartyManager getPartyManager() {
        return partyManager;
    }

    /**
     * Gets the sql class
     *
     * @return the sql class
     */
    public static SQL getSQL() {
        return sql;
    }

    @Override
    public void onEnable() {
        plugin = this;

        // Remove any world that may have been leftover from a crash
        cleanUpWorlds();

        saveDefaultConfig();

        // Create a global random
        globalRandom = new GlobalRandom();

        // Register item glow enchant
        if (Enchantment.getByKey(new NamespacedKey(getPlugin(), "glow")) == null) {
            try {
                Field field = Enchantment.class.getDeclaredField("acceptingNew");
                field.setAccessible(true);
                field.set(null, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Enchantment.registerEnchantment(new ItemGlow(new NamespacedKey(plugin, "glow")));
        }

        // Load Settings
        settings = new Settings();

        // Load Locale
        locale = new Locale();

        // Get Data Method
        if (settings.useSQL()) {
            sql = new SQL();
        }

        // Get new instances
        gameMaster = new GameMaster();
        sql = new SQL();
        friendManager = new FriendManager(settings.isFriendsEnabled());
        scoreManager = new ScoreManager();
        partyManager = new PartyManager();

        // Register events
        getServer().getPluginManager().registerEvents(new ChatManager(), this);
        getServer().getPluginManager().registerEvents(new PlayerJoin(), this);
        getServer().getPluginManager().registerEvents(new PlayerQuit(), this);
        getServer().getPluginManager().registerEvents(new PortalBlocker(), this);
        getServer().getPluginManager().registerEvents(new KitMenuClick(), this);

        // Remove crafting recipes
        getServer().clearRecipes();

        // Set up commands for this plugin
        subCommands.put("help", new HelpSubcommand(this));
        subCommands.put("bypass", new BypassSubcommand(this));
        subCommands.put("gamelist", new GameListSubcommand(this));
        subCommands.put("gameinfo", new GameInfoSubcommand(this));
        CommandManager commandManager = new CommandManager(this, subCommands);
        Objects.requireNonNull(getCommand("gamecore")).setExecutor(commandManager);

        if (friendManager.isFriendsEnabled()) {
            friendSubCommands.put("help", new FriendHelpSubcommand(this));
            friendSubCommands.put("add", new AddFriendSubcommand(this));
            friendSubCommands.put("remove", new RemoveFriendSubcommand(this));
            friendSubCommands.put("accept", new AcceptFriendSubcommand(this));
            friendSubCommands.put("deny", new DenyFriendSubcommand(this));
            friendSubCommands.put("list", new ListFriendSubcommand(this));
            friendSubCommands.put("online", new OnlineFriendsSubcommand(this));
            CommandManager friendCommandManager = new CommandManager(this, friendSubCommands);
            Objects.requireNonNull(getCommand("friends")).setExecutor(friendCommandManager);
        }

        partySubCommands.put("help", new PartyHelpSubcommand(this));
        partySubCommands.put("accept", new PartyAcceptSubcommand(this));
        partySubCommands.put("create", new PartyCreateSubcommand(this));
        partySubCommands.put("deny", new PartyDenySubcommand(this));
        partySubCommands.put("invite", new PartyInviteSubcommand(this));
        partySubCommands.put("kick", new PartyKickSubcommand(this));
        partySubCommands.put("leave", new PartyLeaveSubcommand(this));
        partySubCommands.put("send", new PartySendSubcommand(this));
        partySubCommands.put("list", new PartyListSubcommand(this));
        CommandManager partyCommandManager = new CommandManager(this, partySubCommands);
        Objects.requireNonNull(getCommand("party")).setExecutor(partyCommandManager);

        Objects.requireNonNull(getCommand("leave")).setExecutor(new LeaveCommand());
    }

    @Override
    public void onDisable() {
        friendManager.pushCache();
        scoreManager.pushCache();
        for (Player player : getServer().getOnlinePlayers()) {
            player.kickPlayer("Server Shutting Down");
        }
    }

    private void cleanUpWorlds () {
        File rootFolder = getServer().getWorldContainer().getAbsoluteFile();
        File[] files = rootFolder.listFiles(pathname -> pathname.getName().startsWith("GAMECORE"));

        if (files != null) {
            for (File file : files) {
                DirectoryTools.deleteFolder(file);
            }
        }

    }
}
