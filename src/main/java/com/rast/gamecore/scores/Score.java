package com.rast.gamecore.scores;

import com.rast.gamecore.Game;
import org.bukkit.entity.Player;

public class Score {
    private float value;
    private final String unit;
    private final Player player;
    private final Game game;

    /**
     * A new score object used to keep and manipulate a score and it's units
     *
     * @param initValue The initial value of the score
     * @param unit      The units of the score
     * @param player    The player this score belongs to
     * @param game      The game that this score belongs to
     */
    public Score(float initValue, String unit, Player player, Game game) {
        value = initValue;
        this.unit = unit;
        this.player = player;
        this.game = game;
    }

    /**
     * Set the value of th score overriding the current score
     *
     * @param value the value to set
     */
    public void set(float value) {
        this.value = value;
    }

    /**
     * Gets the score's value
     *
     * @return the score's value
     */
    public float get() {
        return value;
    }

    /**
     * Get the units of this score
     *
     * @return the units as a string
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Get the score as a string with units (value + units)
     *
     * @return The score as a string with units
     */
    public String getString() {
        return value + unit;
    }

    /**
     * Add a value to the current score
     *
     * @param value value to add
     */
    public void add(float value) {
        this.value = this.value + value;
    }

    /**
     * Subtract a value to the current score
     *
     * @param value value to subtract
     */
    public void subtract(float value) {
        this.value = this.value - value;
    }

    /**
     * Multiply a value to the current score
     *
     * @param value value to multiply
     */
    public void multiply(float value) {
        this.value = this.value * value;
    }

    /**
     * Divide the current score by a value
     *
     * @param value value to divide by
     */
    public void divide(float value) {
        this.value = this.value / value;
    }

    /**
     * Get the player that owns this score
     *
     * @return player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Get the game that owns this score
     *
     * @return game
     */
    public Game getGame() {
        return game;
    }
}
