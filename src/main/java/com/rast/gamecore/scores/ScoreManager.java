package com.rast.gamecore.scores;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameCore;
import com.rast.gamecore.data.SQL;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class ScoreManager {

    private final HashMap<Player, HashMap<Game, Set<Score>>> playerScores = new HashMap<>();
    private final HashMap<Player, HashMap<Game, Set<Score>>> playerScoreCache = new HashMap<>();
    private boolean enabled = true;
    private final SQL sql;

    /**
     * Create a new instance of the score manager
     * Only one instance of score manager should be on a server
     */
    public ScoreManager() {
        sql = GameCore.getSQL();
        try {
            Connection connection = sql.openConnection();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'PLAYER_SCORES'), true, false);");
            result.next();
            boolean tableExists = result.getBoolean(1);
            if (!tableExists) {
                GameCore.getPlugin().getLogger().info("Creating table 'PLAYER_SCORES' in the SQL database '" + GameCore.getSettings().getSqlDatabase() + "'");
                statement.executeUpdate("CREATE TABLE " + GameCore.getSettings().getSqlDatabase() +
                        ".PLAYER_SCORES ( UUID CHAR(36) NOT NULL , GAME VARCHAR(255) NOT NULL , VALUE FLOAT NOT NULL DEFAULT '0' , UNIT VARCHAR(255) NOT NULL ) ENGINE = InnoDB;");
            }
            connection.close();
        } catch (SQLException e) {
            enabled = false;
            Bukkit.getLogger().warning("[GameCore] Could not connect to the SQL server and scores will be disabled." +
                    "\nCheck the SQL server host, port, username, and password and make sure they are correct.");
        } catch (ClassNotFoundException e) {
            Bukkit.getLogger().warning("[GameCore] Could not connect to the SQL server and scores will be disabled." +
                    "\nYou may have to install an SQL driver on your system.");
        }
    }

    /**
     * Get the specified player's scores from the SQL server and store them into ScoreManager
     * This method runs asynchronously and may take a while to fetch the scores
     *
     * @param player the player to get the scores of
     */
    public void fetchUserScores(Player player) {
        if (enabled) {
            Runnable runnable = () -> {
                try {
                    Connection connection = sql.openConnection();
                    Statement statement = connection.createStatement();
                    ResultSet result = statement.executeQuery("SELECT * FROM PLAYER_SCORES WHERE (`UUID` = \"" + player.getUniqueId().toString() + "\");");
                    HashMap<Game, Set<Score>> fetchedScores = new HashMap<>();
                    while (result.next()) {
                        Game game = GameCore.getGameMaster().getGame(result.getString("GAME"));
                        float value = result.getFloat("VALUE");
                        String unit = result.getString("UNIT");
                        fetchedScores.computeIfAbsent(game, k -> new HashSet<>());
                        fetchedScores.get(game).add(new Score(value, unit, player, game));
                    }
                    playerScores.put(player, fetchedScores);
                    connection.close();
                } catch (SQLException e) {
                    enabled = false;
                    Bukkit.getLogger().warning("[GameCore] Could not connect to the SQL server and scores will be disabled." +
                            "\nCheck the SQL server host, port, username, and password and make sure they are correct.");
                } catch (ClassNotFoundException e) {
                    Bukkit.getLogger().warning("[GameCore] Could not connect to the SQL server and scores will be disabled." +
                            "\nYou may have to install an SQL driver on your system.");
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();
        }
    }

    /**
     * Remove a player from the ScoreManager
     * Use this when the player leaves to free up memory
     * You can use fetchUserScores() to reload the data
     *
     * @param player the player to remove
     */
    public void removePlayer(Player player) {
        playerScores.remove(player);
        if (enabled) {
            if (playerScoreCache.containsKey(player)) {
                Bukkit.getLogger().info("Starting push of player scores to database");
                Thread thread = new Thread(this::pushCache);
                thread.start();
            }
        }
    }

    /**
     * Refresh the cache after adding or removing a friend
     * Once a threshold set by the GameCore config is reached, the data will be pushed to the sql server
     */
    public void refreshCache() {
        if (enabled) {
            if (GameCore.getSettings().getFriendCacheThreshold() <= playerScoreCache.size()) {
                Bukkit.getLogger().info("Starting push of player scores to database");
                Thread thread = new Thread(this::pushCache);
                thread.start();
            }
        }
    }

    /**
     * Push the data from the cache to the data server and clear the cache
     */
    public void pushCache() {
        if (enabled) {
            try {
                HashMap<Player, HashMap<Game, Set<Score>>> tmpPlayerScoreCache;
                synchronized (GameCore.getPlugin()) {
                    tmpPlayerScoreCache = new HashMap<>(playerScoreCache);
                    playerScoreCache.clear();
                }
                Connection connection = sql.openConnection();
                Statement statement = connection.createStatement();
                for (Map.Entry<Player, HashMap<Game, Set<Score>>> entry : tmpPlayerScoreCache.entrySet()) {
                    UUID playerUUID = entry.getKey().getUniqueId();
                    for (Map.Entry<Game, Set<Score>> subEntry : entry.getValue().entrySet()) {
                        for (Score score : subEntry.getValue()) {
                            ResultSet result = statement.executeQuery("SELECT IF (EXISTS (SELECT * FROM PLAYER_SCORES WHERE UUID = \""
                                    + playerUUID + "\" AND GAME = \"" + subEntry.getKey().getName() + "\" AND UNIT = \"" + score.getUnit() + "\"), true, false);");
                            result.next();
                            boolean scoreExists = result.getBoolean(1);
                            if (!scoreExists) {
                                statement.executeUpdate("INSERT INTO PLAYER_SCORES (UUID, GAME, VALUE, UNIT) VALUES ( \"" + playerUUID + "\" , \"" + subEntry.getKey().getName()
                                        + "\" , \"0\" , \"" + score.getUnit() + "\" )");
                            }
                            statement.executeUpdate("UPDATE PLAYER_SCORES SET VALUE = \""
                                    + score.get() + "\" WHERE UUID = \"" + playerUUID + "\" AND GAME = \"" + subEntry.getKey().getName() + "\" AND UNIT = \"" + score.getUnit() + "\"");
                        }
                    }
                }
                connection.close();
                Bukkit.getLogger().info("Data push complete");
            } catch (SQLException e) {
                enabled = false;
                Bukkit.getLogger().warning("[GameCore] Could not connect to the SQL server and scores will be disabled." +
                        "\nCheck the SQL server host, port, username, and password and make sure they are correct.");
            } catch (ClassNotFoundException e) {
                Bukkit.getLogger().warning("[GameCore] Could not connect to the SQL server and scores will be disabled." +
                        "\nYou may have to install an SQL driver on your system.");
            }
        }
    }

    /**
     * Modify score data
     * Both the SQL server and the ScoreManager's data are edited
     *
     * @param player        The player
     * @param game          The game
     * @param unit          The unit
     * @param value         The value to modify with the score function
     * @param scoreFunction The function used to modify the value
     */
    public void modifyScore(Player player, Game game, String unit, float value, ScoreFunction scoreFunction) {
        if (enabled) {
            Set<Score> scores = playerScores.computeIfAbsent(player, k -> new HashMap<>()).put(game, new HashSet<>());
            if (scores == null) {
                scores = new HashSet<>();
            }
            for (Score score : scores) {
                if (score.getUnit().equals(unit)) {
                    switch (scoreFunction) {
                        case ADD:
                            score.add(value);
                            break;
                        case SUBTRACT:
                            score.subtract(value);
                            break;
                        case MULTIPLY:
                            score.multiply(value);
                            break;
                        case DIVIDE:
                            score.divide(value);
                            break;
                        case SET:
                            score.set(value);
                            break;
                    }
                    playerScores.get(player).put(game, scores);
                    playerScoreCache.computeIfAbsent(player, k -> new HashMap<>()).put(game, scores);
                    return;
                }
            }
            Score score = new Score(0, unit, player, game);
            switch (scoreFunction) {
                case ADD:
                    score.add(value);
                    break;
                case SUBTRACT:
                    score.subtract(value);
                    break;
                case MULTIPLY:
                    score.multiply(value);
                    break;
                case DIVIDE:
                    score.divide(value);
                    break;
                case SET:
                    score.set(value);
                    break;
            }
            scores.add(score);
            playerScores.get(player).put(game, scores);
            playerScoreCache.computeIfAbsent(player, k -> new HashMap<>()).put(game, scores);
            refreshCache();
        }
    }

    /**
     * Get the score for the player of the given unit
     *
     * @param player the player
     * @param game   the score's game
     * @param unit   the unit of the score
     * @return the found score, null if no score is found
     */
    public Score getScore(Player player, Game game, String unit) {
        if (enabled) {
            if (playerScores.get(player) != null) {
                Set<Score> scores = playerScores.get(player).get(game);
                if (scores != null) {
                    for (Score score : scores) {
                        if (score.getUnit().equals(unit)) {
                            return score;
                        }
                    }
                }
            }
        }
        return new Score(0, unit, player, game);
    }

    /**
     * Get the scores for the player of the given game
     *
     * @param player the player
     * @param game   the score's game
     * @return the found score set, null if no scores are found
     */
    public Set<Score> getScores(Player player, Game game) {
        return playerScores.get(player).get(game);
    }

    /**
     * Is scores enabled
     *
     * @return true if scores are enabled, else false
     */
    public boolean isEnabled() {
        return enabled;
    }

}
