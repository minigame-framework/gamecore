package com.rast.gamecore.scores;

public enum ScoreFunction {
    ADD, SUBTRACT, MULTIPLY, DIVIDE, SET
}
