package com.rast.gamecore.friends;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.data.SQL;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class FriendManager {

    private boolean friendsEnabled = false;

    private final SQL sql;

    private final HashMap<Player, Set<UUID>> playerFriends = new HashMap<>();

    private final HashMap<Player, Player> friendRequests = new HashMap<>();

    private final HashMap<UUID, HashMap<UUID, Boolean>> friendUpdateCache = new HashMap<>();

    /**
     * Create a new instance of the friend manager
     * Only one instance of friend manager should be on a server
     *
     * @param friendsEnabled set whether friends is enabled
     */
    public FriendManager(boolean friendsEnabled) {
        sql = GameCore.getSQL();
        if (friendsEnabled) {
            this.friendsEnabled = true;
            try {
                Connection connection = sql.openConnection();
                Statement statement = connection.createStatement();
                ResultSet result = statement.executeQuery("SELECT IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'FRIEND_DATA'), true, false);");
                result.next();
                boolean tableExists = result.getBoolean(1);
                if (!tableExists) {
                    GameCore.getPlugin().getLogger().info("Creating table 'FRIEND_DATA' in the SQL database '" + GameCore.getSettings().getSqlDatabase() + "'");
                    statement.executeUpdate("CREATE TABLE " + GameCore.getSettings().getSqlDatabase() +
                            ".FRIEND_DATA ( UUID CHAR(36) NOT NULL , FRIEND_UUID CHAR(36) NOT NULL ) ENGINE = InnoDB;");
                }
                connection.close();
            } catch (SQLException e) {
                this.friendsEnabled = false;
                Bukkit.getLogger().warning("[Friends] Could not connect to the SQL server and will be disabled." +
                        "\nCheck the SQL server host, port, username, and password and make sure they are correct.");
            } catch (ClassNotFoundException e) {
                Bukkit.getLogger().warning("[Friends] Could not connect to the SQL server and will be disabled." +
                        "\nYou may have to install an SQL driver on your system.");
            }
        }
    }

    /**
     * Get the specified player's friends from the SQL server and store them into FriendManager
     * This method runs asynchronously and may take a while to fetch the request
     *
     * @param player the player to get the friends of
     */
    public void fetchUsersFriends(Player player) {
        if (friendsEnabled) {
            Runnable runnable = () -> {
                try {
                    Connection connection = sql.openConnection();
                    Statement statement = connection.createStatement();
                    ResultSet result = statement.executeQuery("SELECT FRIEND_UUID FROM FRIEND_DATA WHERE (`UUID` = \"" + player.getUniqueId().toString() + "\");");
                    HashSet<UUID> friendUUIDs = new HashSet<>();
                    while (result.next()) {
                        friendUUIDs.add(UUID.fromString(result.getString("FRIEND_UUID")));
                    }
                    friendUUIDs.removeAll(getFriendRemovedFromCache(player));
                    playerFriends.put(player, friendUUIDs);
                    connection.close();
                } catch (SQLException e) {
                    this.friendsEnabled = false;
                    Bukkit.getLogger().warning("[Friends] Could not connect to the SQL server and will be disabled." +
                            "\nCheck the SQL server host, port, username, and password and make sure they are correct.");
                } catch (ClassNotFoundException e) {
                    Bukkit.getLogger().warning("[Friends] Could not connect to the SQL server and will be disabled." +
                            "\nYou may have to install an SQL driver on your system.");
                }
            };
            Thread thread = new Thread(runnable);
            thread.start();
        } else {
            playerFriends.put(player, new HashSet<>());
        }
    }

    /**
     * Add a friend to the player
     * Both the SQL server and the FriendManager's data are edited
     *
     * @param player     the player to give the friend to
     * @param friendUUID the friend's UUID to add to the player
     */
    public void addFriend(Player player, UUID friendUUID) {
        if (friendsEnabled) {
            if (playerFriends.get(player).size() >= GameCore.getSettings().getMaxFriends()) {
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, SoundCategory.MASTER, Float.MAX_VALUE, 1.5f);
                player.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsFull());
                return;
            }
            Set<UUID> uuidSet = playerFriends.get(player);
            if (uuidSet.contains(friendUUID)) {
                return;
            }
            uuidSet.add(friendUUID);
            if (!friendUpdateCache.containsKey(player.getUniqueId())) {
                friendUpdateCache.put(player.getUniqueId(), new HashMap<>());
            }
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, SoundCategory.MASTER, Float.MAX_VALUE, 1.5f);
            friendUpdateCache.get(player.getUniqueId()).put(friendUUID, true);
            refreshCache();
        } else {
            player.sendMessage(GameCore.getLocale().getErrorFriendDisabled());
        }
    }

    /**
     * Remove friend to the player
     * Both the SQL server and the FriendManager's data are edited
     *
     * @param player     the player to remove the friend from
     * @param friendUUID the friend's UUID to remove from the player
     */
    public void removeFriend(Player player, UUID friendUUID) {
        if (friendsEnabled) {
            UUID playerUUID = player.getUniqueId();
            Player friend = Bukkit.getPlayer(friendUUID);
            Set<UUID> uuidSet = playerFriends.get(player);

            if (uuidSet.contains(friendUUID)) {
                uuidSet.remove(friendUUID);

                if (!friendUpdateCache.containsKey(playerUUID)) {
                    friendUpdateCache.put(playerUUID, new HashMap<>());
                }
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, SoundCategory.MASTER, Float.MAX_VALUE, 1.5f);
                player.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsRemoved().replace("%friend%", Objects.requireNonNull(Bukkit.getOfflinePlayer(friendUUID).getName())));
                friendUpdateCache.get(playerUUID).put(friendUUID, false);

                if (!friendUpdateCache.containsKey(friendUUID)) {
                    friendUpdateCache.put(friendUUID, new HashMap<>());
                }
                if (friend != null) {
                    friend.playSound(friend.getLocation(), Sound.ENTITY_VILLAGER_NO, SoundCategory.MASTER, Float.MAX_VALUE, 1.5f);
                    friend.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsRemovedOther().replace("%friend%", player.getName()));
                }
                friendUpdateCache.get(friendUUID).put(playerUUID, false);
                refreshCache();
            }
        } else {
            player.sendMessage(GameCore.getLocale().getErrorFriendDisabled());
        }
    }

    /**
     * Get all players that have unfriended this player that is still in the cache
     *
     * @param player the player that has been unfriended by other players
     * @return a set of UUIDs that have removed this player as their friend
     */
    public HashSet<UUID> getFriendRemovedFromCache(Player player) {

        HashSet<UUID> peopleWhoUnfriended = new HashSet<>();

        for(Map.Entry<UUID, HashMap<UUID, Boolean>> entry : friendUpdateCache.entrySet()) {
            if (entry.getValue().containsKey(player.getUniqueId())) {
                if (entry.getValue().get(player.getUniqueId()).equals(false)) {
                    peopleWhoUnfriended.add(entry.getKey());
                }
            }
        }
        return peopleWhoUnfriended;
    }

    /**
     * Remove a player from the FriendManager
     * Use this when the player leaves to free up memory
     * You can use fetchUsersFriends() to reload the data
     *
     * @param player the player to remove
     */
    public void removeUser(Player player) {
        playerFriends.remove(player);
        if (friendUpdateCache.containsKey(player.getUniqueId())) {
            Bukkit.getLogger().info("Starting push of friend data to database");
            Thread thread = new Thread(this::pushCache);
            thread.start();
        }
    }

    /**
     * Refresh the cache after adding or removing a friend
     * Once a threshold set by the GameCore config is reached, the data will be pushed to the sql server
     */
    public void refreshCache() {
        if (friendsEnabled) {
            if (GameCore.getSettings().getFriendCacheThreshold() <= friendUpdateCache.size()) {
                Bukkit.getLogger().info("Starting push of friend data to database");
                Thread thread = new Thread(this::pushCache);
                thread.start();
            }
        }
    }

    /**
     * Push the data from the cache to the data server and clear the cache
     */
    public void pushCache() {
        if (friendsEnabled) {
            try {
                HashMap<UUID, HashMap<UUID, Boolean>> tmpFriendUpdateCache;
                synchronized (GameCore.getPlugin()) {
                    tmpFriendUpdateCache = new HashMap<>(friendUpdateCache);
                    friendUpdateCache.clear();
                }
                Connection connection = sql.openConnection();
                Statement statement = connection.createStatement();
                for (Map.Entry<UUID, HashMap<UUID, Boolean>> entry : tmpFriendUpdateCache.entrySet()) {
                    UUID playerUUID = entry.getKey();
                    for (Map.Entry<UUID, Boolean> subEntry : entry.getValue().entrySet()) {
                        if (subEntry.getValue()) {
                            statement.executeUpdate("INSERT INTO FRIEND_DATA (UUID, FRIEND_UUID) VALUES (\"" + playerUUID + "\",\"" + subEntry.getKey() + "\")");
                        } else {
                            statement.executeUpdate("DELETE FROM FRIEND_DATA WHERE (UUID = \"" + playerUUID + "\" AND FRIEND_UUID = \"" + subEntry.getKey() + "\")");
                        }
                    }
                }
                connection.close();
                Bukkit.getLogger().info("Data push complete");
            } catch (SQLException e) {
                this.friendsEnabled = false;
                Bukkit.getLogger().warning("[Friends] Could not connect to the SQL server and will be disabled." +
                        "\nCheck the SQL server host, port, username, and password and make sure they are correct.");
            } catch (ClassNotFoundException e) {
                Bukkit.getLogger().warning("[Friends] Could not connect to the SQL server and will be disabled." +
                        "\nYou may have to install an SQL driver on your system.");
            }
        }
    }

    /**
     * Get all the friend UUIDs for the player
     *
     * @param player the player to get the friend UUIDs from
     * @return a HashSet of friend UUIDs
     */
    public Set<UUID> getFriends(Player player) {
        return playerFriends.get(player);
    }

    /**
     * Send a friend request form one player to another to later be accepted
     *
     * @param sender   the player who is sending the friend request
     * @param receiver the player who is receiving the friend request
     */
    public void sendFriendRequest(Player sender, Player receiver) {
        if (friendsEnabled) {
            if (sender.equals(receiver)) {
                sender.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getErrorFriendSelf());
                return;
            }

            if (playerFriends.get(sender).size() >= GameCore.getSettings().getMaxFriends()) {
                sender.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsFull());
                return;
            }
            if (friendRequests.get(sender) != null && friendRequests.get(sender).equals(receiver)) {
                sender.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsAccept().replace("%friend%", receiver.getName()));
                receiver.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsAcceptOther().replace("%friend%", sender.getName()));
                addFriend(sender, receiver.getUniqueId());
                addFriend(receiver, sender.getUniqueId());
                return;
            }
            sender.playSound(sender.getLocation(), Sound.ENTITY_VILLAGER_TRADE, SoundCategory.MASTER, Float.MAX_VALUE, 1.5f);
            receiver.playSound(receiver.getLocation(), Sound.ENTITY_VILLAGER_TRADE, SoundCategory.MASTER, Float.MAX_VALUE, 1.5f);
            sender.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsInvite().replace("%friend%", receiver.getName()));
            receiver.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsInviteOther().replace("%friend%", sender.getName()));
            friendRequests.put(receiver, sender);
        } else {
            sender.sendMessage(GameCore.getLocale().getErrorFriendDisabled());
        }
    }

    /**
     * Accept the latest friend request
     *
     * @param player the player accepting the request
     */
    public void acceptFriendRequest(Player player) {
        if (playerFriends.get(player).size() >= GameCore.getSettings().getMaxFriends()) {
            player.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsFull());
            return;
        }
        Player newFriend = friendRequests.get(player);
        if (newFriend != null) {
            player.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsAccept().replace("%friend%", newFriend.getName()));
            newFriend.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsAcceptOther().replace("%friend%", player.getName()));
            addFriend(player, newFriend.getUniqueId());
            addFriend(newFriend, player.getUniqueId());
        }
        friendRequests.remove(player);
    }

    /**
     * Deny the latest friend request
     *
     * @param player the player denying the request
     */
    public void denyFriendRequest(Player player) {
        Player newFriend = friendRequests.get(player);
        if (newFriend != null) {
            player.playSound(player.getLocation(), Sound.ENTITY_VINDICATOR_AMBIENT, SoundCategory.MASTER, Float.MAX_VALUE, 2);
            newFriend.playSound(newFriend.getLocation(), Sound.ENTITY_VINDICATOR_AMBIENT, SoundCategory.MASTER, Float.MAX_VALUE, 2);
            player.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsDecline().replace("%friend%", newFriend.getName()));
            newFriend.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsDeclineOther().replace("%friend%", player.getName()));
        }
        friendRequests.remove(player);
    }

    /**
     * Check to see weather friends are enabled.
     * This can be different than the value given because friends will disable itself when the SQL server cannot connect.
     *
     * @return a boolean telling weather friends are enabled
     */
    public boolean isFriendsEnabled() {
        return friendsEnabled;
    }

}
