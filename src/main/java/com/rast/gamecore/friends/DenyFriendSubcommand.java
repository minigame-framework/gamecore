package com.rast.gamecore.friends;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class DenyFriendSubcommand extends Subcommand {


    public DenyFriendSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Deny the most recent friend request";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " deny";
    }

    @Override
    public String getPermission() {
        return "gamecore.friends";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        GameCore.getFriendManager().denyFriendRequest(player);

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
