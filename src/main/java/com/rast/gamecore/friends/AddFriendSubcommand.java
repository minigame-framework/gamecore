package com.rast.gamecore.friends;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class AddFriendSubcommand extends Subcommand {


    public AddFriendSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Add a friend to your friend list";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " add";
    }

    @Override
    public String getPermission() {
        return "gamecore.friends";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        if (args.length < 2) {
            player.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendNoPlayerSpecified());
        } else {
            Player friend = Bukkit.getPlayer(args[1]);
            if (friend != null) {
                if (!GameCore.getFriendManager().getFriends(player).contains(friend.getUniqueId())) {
                    GameCore.getFriendManager().sendFriendRequest(player, friend);
                }
            } else {
                player.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsUserNotFound());
            }
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        List<String> tabList = new ArrayList<>();
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            tabList.add(player.getName());
        }
        return tabList;
    }
}
