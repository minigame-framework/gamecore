package com.rast.gamecore.friends;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

public class ListFriendSubcommand extends Subcommand {


    public ListFriendSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Lists people you are friends with";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " list";
    }

    @Override
    public String getPermission() {
        return "gamecore.friends";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        player.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsList());
        Set<UUID> friendsUUIDs = GameCore.getFriendManager().getFriends(player);
        if (friendsUUIDs.size() == 0) {
            player.sendMessage(ChatColor.GRAY + "You have no friends.");
            return true;
        }

        StringBuilder builder = new StringBuilder();
        Iterator<UUID> iterator = friendsUUIDs.iterator();
        while (iterator.hasNext()) {
            UUID uuid = iterator.next();
            builder.append(ChatColor.GRAY).append(Bukkit.getOfflinePlayer(uuid).getName());
            if (iterator.hasNext()) {
                builder.append(ChatColor.DARK_GRAY).append(", ");
            }
            if (builder.length() > 20) {
                player.sendMessage(builder.toString());
                builder = new StringBuilder();
            }
        }
        String finalString  = builder.toString();
        if (!finalString.isEmpty()) {
            player.sendMessage(finalString);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
