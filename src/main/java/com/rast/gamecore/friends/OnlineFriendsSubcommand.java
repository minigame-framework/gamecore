package com.rast.gamecore.friends;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class OnlineFriendsSubcommand extends Subcommand {

    public OnlineFriendsSubcommand(JavaPlugin plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Lists your online friends";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " online";
    }

    @Override
    public String getPermission() {
        return "gamecore.friends";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        player.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendsListOnline());
        Set<UUID> friendsUUIDs = GameCore.getFriendManager().getFriends(player);
        boolean addedFriend = false;

        StringBuilder builder = new StringBuilder();
        Iterator <UUID> iterator = friendsUUIDs.iterator();
        while (iterator.hasNext()) {
            UUID uuid = iterator.next();
            OfflinePlayer friend = Bukkit.getOfflinePlayer(uuid);
            if (friend.isOnline()) {
                addedFriend = true;
                builder.append(ChatColor.GRAY).append(friend.getName());
                if (iterator.hasNext()) {
                    builder.append(ChatColor.DARK_GRAY).append(", ");
                }
                if (builder.length() > 20) {
                    player.sendMessage(builder.toString());
                    builder = new StringBuilder();
                }
            }
        }
        if (!addedFriend) {
            player.sendMessage(ChatColor.GRAY + "You have no friends online.");
            return true;
        }

        player.sendMessage(builder.toString());
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }
}
