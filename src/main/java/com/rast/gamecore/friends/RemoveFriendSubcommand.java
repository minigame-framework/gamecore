package com.rast.gamecore.friends;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.util.Subcommand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RemoveFriendSubcommand extends Subcommand {


    public RemoveFriendSubcommand(GameCore plugin) {
        super(plugin);
    }

    @Override
    public String getHelp() {
        return "Remove a friend to your friend list";
    }

    @Override
    public String getUsage(String label) {
        return "/" + label + " remove";
    }

    @Override
    public String getPermission() {
        return "gamecore.friends";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        if (args.length < 2) {
            player.sendMessage(GameCore.getLocale().getFriendsPrefix() + GameCore.getLocale().getFriendNoPlayerSpecified());
        } else {
            GameCore.getFriendManager().removeFriend(player, Bukkit.getOfflinePlayer(args[1]).getUniqueId());
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
            return null;

        List<String> tabList = new ArrayList<>();
        for (UUID friendUUID : GameCore.getFriendManager().getFriends((Player) sender)) {
            tabList.add(Bukkit.getOfflinePlayer(friendUUID).getName());
        }
        return tabList;
    }
}
