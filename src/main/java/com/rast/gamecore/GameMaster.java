package com.rast.gamecore;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GameMaster {

    private final HashMap<String, Set<Player>> playerGroups = new HashMap<>();
    private final HashMap<Player, Game> playerGame = new HashMap<>();
    private final Set<Game> games = new HashSet<>();

    /**
     * Creates a player group with the name provided
     *
     * @param name the name for the group
     */
    public void createPlayerGroup(String name) {
        playerGroups.put(name, new HashSet<>());
    }

    /**
     * Adds a player to a player group
     *
     * @param name   the name of the group to add to
     * @param player the player to add to the group
     */
    public void addToPlayerGroup(String name, Player player) {
        Set<Player> pSet = playerGroups.get(name);
        pSet.add(player);
        playerGroups.replace(name, pSet);
    }

    /**
     * Gets the players contained in a group
     *
     * @param name the name of the group to get
     * @return the players in the group or return null if it does not exist
     */
    public Set<Player> getGroupPlayers(String name) {
        return playerGroups.get(name);
    }

    /**
     * Removes the player from a specified group
     *
     * @param name   the group to remove the player from
     * @param player the player to remove
     */
    public void removePlayerFromGroup(String name, Player player) {
        playerGroups.get(name).remove(player);
    }

    /**
     * Remove a group from the player groups
     *
     * @param name the name of the group to remove
     */
    public void removePlayerGroup(String name) {
        playerGroups.remove(name);
    }

    /**
     * Finds what groups the player is in
     *
     * @param player the player to find in the groups
     * @return a list of groups the player is in
     */
    public Set<String> getPlayerGroups(Player player) {
        Set<String> groups = new HashSet<>();
        for (Map.Entry<String, Set<Player>> entry : playerGroups.entrySet()) {
            if (entry.getValue().contains(player)) {
                groups.add(entry.getKey());
            }
        }
        return groups;
    }

    /**
     * Set the game to the player
     *
     * @param player the player to set the game to
     * @param game   the game to set to the player
     */
    public void setPlayerGame(Player player, Game game) {
        playerGame.put(player, game);
    }

    /**
     * Removes the game from the player.
     * This will results in null if the player is not registered with a game
     *
     * @param player the player remove
     */
    public void removePlayerGame(Player player) {
        playerGame.remove(player);
    }

    /**
     * Gets the game that the player was assigned
     *
     * @param player player to get the game of
     * @return the game the player is in
     */
    public Game getPlayerGame(Player player) {
        return playerGame.get(player);
    }

    /**
     * Register the game with the Game Master
     *
     * @param game the game to register
     */
    public void registerGame(Game game) {
        games.add(game);
    }

    /**
     * Remove the game from the Game Master's registry
     *
     * @param game the game to remove
     */
    public void removeGame(Game game) {
        games.remove(game);
    }

    /**
     * Get all the games registered with the Game Master
     *
     * @return the games in the Game Master registry
     */
    public Set<Game> getGames() {
        return games;
    }

    /**
     * Get a game based on it's name
     *
     * @param name the name of the game to get
     * @return the game or null if no game exists with that name
     */
    public Game getGame(String name) {
        for (Game game : games) {
            if (game.getName().equals(name)) {
                return game;
            }
        }
        return null;
    }

}
