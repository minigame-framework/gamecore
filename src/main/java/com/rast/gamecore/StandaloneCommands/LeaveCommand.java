package com.rast.gamecore.standaloneCommands;

import com.rast.gamecore.Game;
import com.rast.gamecore.GameCore;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LeaveCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (!(sender instanceof Player))
            return false;

        final Player player = (Player) sender;

        Game game = GameCore.getGameMaster().getPlayerGame(player);
        if (game != null) {
            game.pullPlayer(player);
        }
        GameCore.getGameMaster().getGame(GameCore.getSettings().getDefaultGame()).sendPlayer(player);
        return true;
    }
}
