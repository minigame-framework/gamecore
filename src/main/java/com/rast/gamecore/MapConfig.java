package com.rast.gamecore;

import org.bukkit.Location;

public abstract class MapConfig {

    private final String name;
    private final int maxPlayers;
    private final Location gameSpawn;

    /**
     * Create a map config which has a name and max players.
     *
     * @param name name of the map which should be the same as the map folder
     * @param maxPlayers the maximum amount of players displayed by gamecore
     * @param gameSpawn the main spawn for the game which should be in the arena (for spectators)
     */
    public MapConfig(String name, int maxPlayers, Location gameSpawn) {
        this.name = name;
        this.maxPlayers = maxPlayers;
        this.gameSpawn = gameSpawn;
    }

    /**
     * Get the name of map for this map config.
     *
     * @return the name of the map
     */
    public String getName() {
        return name;
    }

    /**
     * Get the maximum players allowed on this map not including spectators.
     *
     * @return the max players allowed
     */
    public int getMaxPlayers() {
        return maxPlayers;
    }

    /**
     * Get the game spawn for the this map.
     *
     * @return the game spawn
     */
    public Location getGameSpawn() {
        return gameSpawn;
    }
}
